<?php

require __DIR__ . '/../conf.d/configparser.php';
require __DIR__ .  '/lib/ralph.php';
require __DIR__ .  '/lib/imageUploader.php';

use Respect\Validation\Validator as v;

$container = $app->getContainer();

/* ========================================
            twig templating
   ======================================== */

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__.'/../templates', [
        'cache' => false, //'cache'
        'debug' => true
    ]);
    $view->addExtension(new \Slim\Views\TwigExtension(
        $container['router'],
        $container['request']->getUri()
    ));
    $view->addExtension(new Twig_Extension_Debug());

    return $view;
};
$container['view']['session'] = $_SESSION;
$container['view']['footer_countdown'] = time_until_day_ends();

/* ========================================
            Monolog logging
   ======================================== */
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

/* ========================================
            Login validation
   ======================================== */

$container['loginValidation'] = function () {

  $emailValidator = v::notEmpty()->email()->setName('Email');
  $passwordValidator = v::notEmpty()->length(7, null)->setName('Password');
  $validators = array(
    'user_email' => $emailValidator,
    'user_password' => $passwordValidator
  );

  return new \DavidePastore\Slim\Validation\Validation($validators);
};

/* ========================================
            Register validation
   ======================================== */

$container['registerValidation'] = function () {

  $emailValidator = v::notEmpty()->email()->setName('Email');
  $passwordValidator = v::notEmpty()->length(7, null)->setName('Password');
  $validators = array(
    'user_email' => $emailValidator,
    'user_password' => $passwordValidator,
    'user_password_confirm' => $passwordValidator
  );

  return new \DavidePastore\Slim\Validation\Validation($validators);
};

/* ========================================
              Custom 404
   ======================================== */

$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
            return $c->view->render($response, '404.twig') 
                ->withStatus(404)
                ->withHeader('Content-Type', 'text/html');
        };
};

function time_until_day_ends()
{
    $today = time();
    $end_today = strtotime("tomorrow")-1;
    $time_left_in_day = $end_today - $today;
    $hours = floor($time_left_in_day / 3600);
    $minutes = floor($time_left_in_day / 60 % 60);

    if((int)$hours >= 1){
        return 'Day ends in '.$hours.' hours, '.$minutes.' minutes.';
    }else{
        return 'Day ends in '.$minutes.' minutes.';
    }
}