<?php
$r = new \Ralph\api();
/*
    Search route, can go to both /clan and /profile
*/
        
$app->map(['GET','POST'], '/search', function ($request, $response, $args) use ($r) {

    if ($request->isPost()) {

        $post = (object)$request->getParams();
        
        //retrieve the search term.
        $search = $post->search; //retrieve the search term.
        $search = trim($search); //we can trim $search, as a username cannot start or end with a space.
        $search = preg_replace('/[^\w -]+/', '', $search); //filter out all non-alphanumeric characters besides - and spaces.
        $search_type = $post->search_type; //will be either "player" or "clan"
        $_SESSION['search_type'] = $post->search_type; //store what the player searched for

        if (strlen($search) > 0 && (strlen($search) <= 12 && $search_type == "player" || strlen($search) <= 20 && $search_type == "clan")) {
            if($search_type == "player"){

                if (!get_player_id($search)) {

                    $last_name_check = get_player_name_by_last_name($search);
                    if(!$last_name_check){

                        $player_details = $r->get_details($search);
                        if (isset($player_details->clan)) { //the player exists and has a clan

                            $clan = get_clan_id($player_details->clan);
                            if ($clan === false) {
                                $clan = add_clan($player_details->clan);
                            }

                            add_player($search, $clan);
                            return $response->withRedirect('/profile/'.clean($search));
                        }else{
                            $player_profile = $r->get_profile($search);
                            if (!isset($player_profile->error)) {
                                add_player($search, 0);//add a pleb (clan-less player)
                                return $response->withRedirect('/profile/'.clean($search));
                            }else{
                                $args['error'] = 'That player does not exist.';
                                return $this->view->fetch('search.twig', $args);
                            }
                        }
                    }else{
                        return $response->withRedirect('/profile/'.clean($last_name_check)); //found old name -> go to profile
                    }

                }else{
                    return $response->withRedirect('/profile/'.clean($search));
                }

            }elseif($search_type == "clan"){
                if (!get_clan_id($search)) {
                    $clan_details = $r->get_clan_list($search);
                    if (!isset($clan_details->error)) {
                        add_clan($search);
                        return $response->withRedirect('/clan/'.clean($search));
                    }else{
                        $args['error'] = 'That clan does not exist.';
                        return $this->view->fetch('search.twig', $args);
                    }
                }else{
                    return $response->withRedirect('/clan/'.clean($search));
                }
            }

        } else {
            $args['error'] = 'Your search was either too short or too long. Enter a valid length. </br>(1-12 for players, 1-20 for clans)';
            return $this->view->fetch('search.twig', $args);
        }

    } else {
        return $response->withRedirect('/404');
    }
});

/*
    Player profile
*/
$app->get('/profile/{name}', function ($request, $response, $args) use($r) {
    $this->logger->info("Slim-Skeleton '/profile/[{name}]' route");

    //filter the name
    $name = $args['name']; //retrieve the name term.
    $name = trim($name); //we can trim $name, as a username cannot start or end with a space.
    $name = preg_replace('/[^\w -]+/', '', $name); //filter out all non-alphanumeric characters besides - and spaces.

    $args['player'] = get_player_by_name($name);

    if (!$args['player']) {
        //player not found -> add him and his clan to the db
        $last_name_check = get_player_name_by_last_name($name);
        if(!$last_name_check){

            $player_details = $r->get_details($name);
            if (isset($player_details->clan)) { //the player exists and has a clan

                $clan = get_clan_id($player_details->clan);
                if ($clan === false) {
                    $clan = add_clan($player_details->clan);
                }

                add_player($name, $clan);
                return $response->withRedirect('/profile/'.clean($name));
            }else{
                $player_profile = $r->get_profile($name);
                if (!isset($player_profile->error)) {
                    add_player($name, 0);//add a pleb (clan-less player)
                    return $response->withRedirect('/profile/'.clean($name));
                }else{
                    $args['error'] = 'That player does not exist.';
                    return $this->view->fetch('error.twig', $args);
                }
            }
        }else{
            return $response->withRedirect('/profile/'.clean($last_name_check)); //found old name -> go to profile
        }
    } else {
        //start the update asap
        $path_to_update_script = dirname(__DIR__)."/cron/update_profile.php";
        exec("php ".$path_to_update_script." ".$args['player']->pl_id." > /dev/null 2>/dev/null &");

        //ironman check
        if($args['player']->pl_type == 0){
            //check for hcim
            $hcim_check = $r->check_hcim($args['player']->pl_name);
            if(!$hcim_check){
                $im_check = $r->check_im($args['player']->pl_name);
                if(!$im_check){
                    set_type($args['player']->pl_id, 1);
                }else{
                    set_type($args['player']->pl_id, 2);
                }
            }else{  
                set_type($args['player']->pl_id, 3);
            }
        }
        cache_avatar($args['player']);
        $player_titles = get_player_available_titles($args['player']->pl_id);
        $args['default_player_title'] = count($player_titles) > 0 ? end($player_titles) : '';
        $args['all_player_titles'] = $player_titles;
        $args['last_update'] = intval(date('i', (time() - $args['player']->pl_last_updated)));
        $args['clan'] = get_clan($args['player']->pl_clan_id);
        $args['skills'] = get_skills();

        $args['skill_values'] = get_skill_values($args['player']->pl_id);
        if (!empty($args['skill_values'])) {
            $args['overall']['total_percentage_to_max'] = 0;
            $args['overall']['total_percentage_to_true_max'] = 0;
            $total_99s = 0;
            $total_120s = 0;
            foreach ($args['skill_values'] as $skill_index => $skill) {
                $xp_to_99 = $args['skills'][$skill_index]->sk_is_elite == true ? 360735110 : 130344310;
                $xp_to_120 = $args['skills'][$skill_index]->sk_is_elite == true ? 806186540 : 1042731670;
            
                $args['overall']['total_percentage_to_max'] += min($skill->ex_value, $xp_to_99);
                $args['overall']['total_percentage_to_true_max'] += min($skill->ex_value, $xp_to_120);
                
                $total_99s += $xp_to_99; //26*130344310+1*360735110
                $total_120s += $xp_to_120; //26*1042731670+1*806186540

                //Retrieve virtual level if the level is above max.
                if(!$args['skills'][$skill_index]->sk_is_elite && $args['skill_values'][$skill_index]->ex_level == 99)
                {
                    $args['skill_values'][$skill_index]->ex_level = get_virtual_level($args['skill_values'][$skill_index]->ex_value / 10);                
                }
                else if($args['skills'][$skill_index]->sk_is_elite && $args['skill_values'][$skill_index]->ex_level == 120)
                {
                    $args['skill_values'][$skill_index]->ex_level = get_elite_virtual_level($args['skill_values'][$skill_index]->ex_value / 10);                                
                }

            }

            $args['overall']['percentage_to_max'] = ($args['overall']['total_percentage_to_max'] / $total_99s) * 100;
            $args['overall']['percentage_to_true_max'] = ($args['overall']['total_percentage_to_true_max'] / $total_120s) * 100;
            $args['player_is_confirmed'] = check_confirmed_player($args['player']->pl_id);

            $args['exp_today'] = get_exp_logs($args['player']->pl_id, new DateTime('today'));
            $args['exp_yesterday'] = get_exp_logs($args['player']->pl_id, new DateTime('yesterday'));
            $args['exp_this_week'] = get_exp_logs($args['player']->pl_id, new DateTime('-1 week sunday'), new DateTime('today'));
            // Overall calculations
            
            $args['overall']['levels_gained'] = 0;
            $args['overall']['ranks_gained'] = 0;
            $args['overall']['experience_today'] = 0;
            $args['overall']['experience_yesterday'] = 0;
            $args['overall']['experience_week'] = 0;
            foreach ($args['exp_today'] as $log) {
                $args['overall']['experience_today'] += $log->experience;
                $args['overall']['levels_gained'] += $log->level;
                $args['overall']['ranks_gained'] += $log->rank;
            }
            foreach ($args['exp_yesterday'] as $log) {
                $args['overall']['experience_yesterday'] += $log->experience;
            }
            foreach ($args['exp_this_week'] as $log) {
                $args['overall']['experience_week'] += $log->experience;
            }
        }
        
        return $this->view->fetch('profile/profile.twig', $args);
    }
});

$app->get('/profile/{name}/skills', function ($request, $response, $args) use($r) {

    $args['player'] = get_player_by_name($args['name']);
    $args['skills'] = get_skills();
    $args['skill_values'] = get_skill_values($args['player']->pl_id);
    if (!empty($args['skill_values'])) {
        $args['overall']['total_percentage_to_max'] = 0;
        $args['overall']['total_percentage_to_true_max'] = 0;
        $total_99s = 0;
        $total_120s = 0;
        foreach ($args['skill_values'] as $skill_index => $skill) {
            $xp_to_99 = $args['skills'][$skill_index]->sk_is_elite == true ? 360735110 : 130344310;
            $xp_to_120 = $args['skills'][$skill_index]->sk_is_elite == true ? 806186540 : 1042731670;
        
            $args['overall']['total_percentage_to_max'] += min($skill->ex_value, $xp_to_99);
            $args['overall']['total_percentage_to_true_max'] += min($skill->ex_value, $xp_to_120);
            
            $total_99s += $xp_to_99; //26*130344310+1*360735110
            $total_120s += $xp_to_120; //26*1042731670+1*806186540

            //Retrieve virtual level if the level is above max.
            if(!$args['skills'][$skill_index]->sk_is_elite && $args['skill_values'][$skill_index]->ex_level == 99)
            {
                $args['skill_values'][$skill_index]->ex_level = get_virtual_level($args['skill_values'][$skill_index]->ex_value / 10);                
            }
            else if($args['skills'][$skill_index]->sk_is_elite && $args['skill_values'][$skill_index]->ex_level == 120)
            {
                $args['skill_values'][$skill_index]->ex_level = get_elite_virtual_level($args['skill_values'][$skill_index]->ex_value / 10);                                
            }
        }

        


        $args['overall']['percentage_to_max'] = ($args['overall']['total_percentage_to_max'] / $total_99s) * 100;
        $args['overall']['percentage_to_true_max'] = ($args['overall']['total_percentage_to_true_max'] / $total_120s) * 100;
        $args['player_is_confirmed'] = check_confirmed_player($args['player']->pl_id);

        $args['exp_today'] = get_exp_logs($args['player']->pl_id, new DateTime('today'));
        $args['exp_yesterday'] = get_exp_logs($args['player']->pl_id, new DateTime('yesterday'));
        $args['exp_this_week'] = get_exp_logs($args['player']->pl_id, new DateTime('-1 week sunday'), new DateTime('today'));
        // Overall calculations
        
        $args['overall']['levels_gained'] = 0;
        $args['overall']['ranks_gained'] = 0;
        $args['overall']['experience_today'] = 0;
        $args['overall']['experience_yesterday'] = 0;
        $args['overall']['experience_week'] = 0;
        foreach ($args['exp_today'] as $log) {
            $args['overall']['experience_today'] += $log->experience;
            $args['overall']['levels_gained'] += $log->level;
            $args['overall']['ranks_gained'] += $log->rank;
        }
        foreach ($args['exp_yesterday'] as $log) {
            $args['overall']['experience_yesterday'] += $log->experience;
        }
        foreach ($args['exp_this_week'] as $log) {
            $args['overall']['experience_week'] += $log->experience;
        }
    }

    return $this->view->fetch('profile/subpages/skills.twig', $args);
});

$app->get('/profile/{name}/drops', function ($request, $response, $args) use($r) {

    $args['player'] = get_player_by_name($args['name']);
    $args['pinned_drops'] = get_pinned_drops($args['player']->pl_id);
    $args['drops'] = get_drops($args['player']->pl_id);
    foreach ($args['drops'] as $drop) {
        cache_item_img($drop);
    }
    return $this->view->fetch('profile/subpages/drops.twig', $args);
});

$app->get('/profile/{name}/quests', function ($request, $response, $args) use($r) {

    $args['player'] = get_player_by_name($args['name']);
    $args['quest_difficulties'] = ['Novice', 'Intermediate', 'Experienced', 'Master', 'Grandmaster', 'Special'];
    $args['quests'] = $r->get_quests($args['name']);
    return $this->view->fetch('profile/subpages/quests.twig', $args);
});

$app->get('/profile/{name}/activity', function ($request, $response, $args) use($r) {

    $args['skills'] = get_skills();
    return $this->view->fetch('profile/subpages/activity.twig', $args);
});

/*
    Clan page
*/
$app->get('/clan/{name}', function ($request, $response, $args) use($app) {
  
    $clan_id = get_clan_id($args['name']);

    //set default options to members load correctly
    //handle the sorting parameters
    $args['page'] = 1;
    $args['sort'] = 'exp_today';
    $args['sort_order'] = 'desc';
    $sort = $args['sort'];
    $sort_order = $args['sort_order'];
    
    if (!$clan_id) {
        return $response->withRedirect('/error');
    } else {

        if(!isset($_SESSION['clan_id']) || $_SESSION['clan_id'] != $clan_id){
            //Initialize the clan session variable
            $clan_data = [];
            $clan_data['clan'] = get_clan($clan_id);
            $clan_data['clan']->logo = file_exists(__DIR__.'/../public/img/clans/'.clean_underscore($args['name']).'.png') ? clean_underscore($args['name']) : 'nopic';

            //Loop over the members and set the global clan data
            $total_clan_exp = 0;
            $total_clan_exp_today = 0;
            $members = get_clan_members($clan_id);

            $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
            $reset_date = date('l ga', strtotime($weekdays[$clan_data['clan']->cl_reset_day].' '.$clan_data['clan']->cl_reset_hour.':00'));
            $last_reset = strtotime('last '.$reset_date);

            $daily_exp_values = get_clan_exp_logs($clan_id, new DateTime('now'));

            foreach ($members as &$member) { 
                $total_clan_exp += $member->pl_clan_exp;

                if($member->pl_last_activity > strtotime('-1 week')){
                    $member->active = true;
                }else{
                    $member->active = false;
                }

               if(isset($daily_exp_values[$member->pl_id])){
                   $member->exp_today = $daily_exp_values[$member->pl_id]->experience;
                   $member->levels_today = $daily_exp_values[$member->pl_id]->level;
                   $member->ranks_today = $daily_exp_values[$member->pl_id]->rank;
               }else{
                   $member->exp_today = 0;
                   $member->levels_today = 0;
                   $member->ranks_today = 0;
               }
                $total_clan_exp_today += $member->exp_today; 
                
                if($member->pl_last_cap > $last_reset){
                    $member->has_capped = true;
                }else{
                    $member->has_capped = false;
                }

                $clan_ranks = ["Owner", "Deputy Owner", "Overseer", "Coordinator", "Organiser", "Admin", "General", "Captain", "Lieutenant", "Sergeant", "Corporal", "Recruit"];
                $member->pl_clan_rank_nr = array_search($member->pl_clan_rank, $clan_ranks);
            }

            //sort on daily exp by default
            usort($members, function($a, $b) use ($sort, $sort_order) {
                if($sort_order == "desc"){
                    return $b->$sort <=> $a->$sort;
                }else{
                    return $a->$sort <=> $b->$sort;
                }
            });

            $clan_data['members'] = $members;
            $clan_data['clan']->member_count = count($members);
            $clan_data['clan']->total_clan_exp = $total_clan_exp;
            $clan_data['clan']->total_clan_exp_today = $total_clan_exp_today;
            $clan_data['pages'] = ceil(count($members) / 20);
            $clan_data['last_reset'] = strtotime('last '.$reset_date);

            //Store it
            $_SESSION['clan_id'] = $clan_id;
            $_SESSION['clan'] = $clan_data;
        }

        //Merge it into $args
        $args = array_merge($args, $_SESSION['clan']);

        $args['member_slice'] = array_slice($args['members'], 0, 19);

        return $this->view->fetch('clan/clan.twig', $args);
    }
});

$app->map(['GET','POST'], '/clan/{name}/members[/{page}[/{sort}/{sort_order}]]', function ($request, $response, $args) {

    $members_per_page = 20;
    $clan_id = get_clan_id($args['name']);
    $args['skills'] = get_skills();

    //handle the page parameter
    if (isset($args['page'])) {
        $page = $args['page'] - 1;
        $bottom_limit = $page*$members_per_page; //0, 20, 40, 60
    } else {
        $args['page'] = 1;
        $bottom_limit = 0;
    }

    //handle the sorting parameters
    if (!isset($args['sort'])) {
        $args['sort'] = 'exp_today';
        $args['sort_order'] = 'desc';
    }

    $sort = $args['sort'];
    $sort_order = $args['sort_order'];

    if (!$clan_id) {
        return $response->withRedirect('/error');
    } else {
        if(!isset($_SESSION['clan_id']) || $_SESSION['clan_id'] != $clan_id || !isset($_SESSION['clan']['members'])){

            $members = get_clan_members($clan_id);

            $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
            $reset_date = date('l ga', strtotime($weekdays[$_SESSION['clan']['clan']->cl_reset_day].' '.$_SESSION['clan']['clan']->cl_reset_hour.':00'));
            $last_reset = strtotime('last '.$reset_date);

            $daily_exp_values = get_clan_exp_logs($clan_id, new DateTime('now'));

            foreach ($members as &$member) { 
                
                if($member->pl_last_activity > strtotime('-1 week')){
                    $member->active = true;
                }else{
                    $member->active = false;
                }

                if(isset($daily_exp_values[$member->pl_id])){
                    $member->exp_today = $daily_exp_values[$member->pl_id]->experience;
                    $member->levels_today = $daily_exp_values[$member->pl_id]->level;
                    $member->ranks_today = $daily_exp_values[$member->pl_id]->rank;
                }else{
                    $member->exp_today = 0;
                    $member->levels_today = 0;
                    $member->ranks_today = 0;
                }
                
                if($member->pl_last_cap > $last_reset){
                    $member->has_capped = true;
                }else{
                    $member->has_capped = false;
                }

                //set the numeral rank for sorting
                $clan_ranks = ["Owner", "Deputy Owner", "Overseer", "Coordinator", "Organiser", "Admin", "General", "Captain", "Lieutenant", "Sergeant", "Corporal", "Recruit"];
                $member->pl_clan_rank_nr = array_search($member->pl_clan_rank, $clan_ranks);
            }

            //update the session
            $_SESSION['clan']['members'] = $members;
        }

        //Load the clan data from session into template args
        $args = array_merge($args, $_SESSION['clan']);

        //sort on daily exp by default
        usort($args['members'], function($a, $b) use ($sort, $sort_order) {
            if($sort_order == "desc"){
                return $b->$sort <=> $a->$sort;
            }else{
                return $a->$sort <=> $b->$sort;
            }
        });

        //slice members for pagination
        $args['member_slice'] = array_slice($args['members'], $bottom_limit, ($members_per_page-1));

    return $this->view->fetch('clan/subpages/members.twig', $args);
    }
});

$app->get('/clan/{name}/drops', function ($request, $response, $args) {

    $clan_id = get_clan_id($args['name']);
    
    if (!$clan_id) {
        return $response->withRedirect('/error');
    } else {
        if(!isset($_SESSION['clan_id']) || $_SESSION['clan_id'] != $clan_id || !isset($_SESSION['clan']['drops'])){
            echo "new load";
            //use session data
            $drops = get_clan_drops($clan_id);//0.013
            $cached_ids = [];
            foreach ($drops as $drop) {
                if(!in_array($drop->dr_img_id, $cached_ids)){
                    cache_item_img($drop); //1.2~60s
                    $cached_ids[] = $drop->dr_img_id;
                }
            }

            $_SESSION['clan']['drops'] = $drops;
        }

        //Load the clan data from session into template args
        $args = array_merge($args, $_SESSION['clan']);

    return $this->view->fetch('clan/subpages/drops.twig', $args);
    }
});

$app->get('/clan/{name}/competitions', function ($request, $response, $args) {

    $clan_id = get_clan_id($args['name']);
    $args['skills'] = get_skills();

    if (!$clan_id) {
        return $response->withRedirect('/error');
    } else {
        if(!isset($_SESSION['clan_id']) || $_SESSION['clan_id'] != $clan_id || !isset($_SESSION['clan']['competitions'])){

            $_SESSION['clan']['competitions'] = get_clan_competitions($clan_id);
        }

        //Load the clan data from session into template args
        $args = array_merge($args, $_SESSION['clan']);

    return $this->view->fetch('clan/subpages/competitions.twig', $args);
    }
});

$app->get('/clan/{name}/activity', function ($request, $response, $args) {

    $args['skills'] = get_skills();
    $clan_id = get_clan_id($args['name']);

    if (!$clan_id) {
        return $response->withRedirect('/error');
    } else {
        if(!isset($_SESSION['clan_id']) || $_SESSION['clan_id'] != $clan_id || !isset($_SESSION['clan']['activity'])){

            $_SESSION['clan']['activity'] = get_clan_achievements($clan_id);
        }

        //Load the clan data from session into template args
        $args = array_merge($args, $_SESSION['clan']);

    return $this->view->fetch('clan/subpages/activity.twig', $args);
    }
});

/*
    Clan dashboard
*/

$app->map(['GET','POST'], '/dashboard/{name}', function ($request, $response, $args) {

    $args['page'] = 1;
    $members_per_page = 20;

    //handle the page parameter
    if (isset($args['page'])) {
        $page = $args['page'] - 1;
        $bottom_limit = $page*$members_per_page; //0, 20, 40, 60
    } else {
        $args['page'] = 1;
        $bottom_limit = 0;
    }

    //handle the sorting parameters
    if (!isset($args['sort'])) {
        $args['sort'] = 'pl_clan_rank';
        $args['sort_order'] = 'desc';
    }

    $sort = $args['sort'];
    $sort_order = $args['sort_order'];

    $clan_id = get_clan_id($args['name']);
    $args['clan'] = get_clan($clan_id);

    $members = get_clan_members($clan_id);

    $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    $reset_date = date('l ga', strtotime($weekdays[$args['clan']->cl_reset_day].' '.$args['clan']->cl_reset_hour.':00'));
    $last_reset = strtotime('last '.$reset_date);


    foreach ($members as &$member) { 
        
        if($member->pl_last_activity > strtotime('-1 week')){
            $member->active = true;
        }else{
            $member->active = false;
        }

        if($member->pl_last_cap > $last_reset){
            $member->has_capped = true;
        }else{
            $member->has_capped = false;
        }

        //set the numeral rank for sorting
        $clan_ranks = ["Owner", "Deputy Owner", "Overseer", "Coordinator", "Organiser", "Admin", "General", "Captain", "Lieutenant", "Sergeant", "Corporal", "Recruit"];
        $member->pl_clan_rank_nr = array_search($member->pl_clan_rank, $clan_ranks);
    }

    //sort on daily exp by default
    usort($members, function($a, $b) use ($sort, $sort_order) {
        if($sort_order == "desc"){
            return $b->$sort <=> $a->$sort;
        }else{
            return $a->$sort <=> $b->$sort;
        }
    });

    //slice members for pagination
    $args['member_slice'] = array_slice($members, $bottom_limit, ($members_per_page-1));

    return $this->view->fetch('dashboard/dashboard.twig', $args);
})->add($owner_only);

$app->map(['GET','POST'], '/dashboard/{name}/members[/{page}[/{sort}/{sort_order}]]', function ($request, $response, $args) {

    $members_per_page = 20;

    //handle the page parameter
    if (isset($args['page'])) {
        $page = $args['page'] - 1;
        $bottom_limit = $page*$members_per_page; //0, 20, 40, 60
    } else {
        $args['page'] = 1;
        $bottom_limit = 0;
    }

    //handle the sorting parameters
    if (!isset($args['sort'])) {
        $args['sort'] = 'pl_clan_rank';
        $args['sort_order'] = 'desc';
    }

    $sort = $args['sort'];
    $sort_order = $args['sort_order'];

    $clan_id = get_clan_id($args['name']);
    $args['clan'] = get_clan($clan_id);
    $members = get_clan_members($clan_id);

    $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    $reset_date = date('l ga', strtotime($weekdays[$args['clan']->cl_reset_day].' '.$args['clan']->cl_reset_hour.':00'));
    $last_reset = strtotime('last '.$reset_date);

    foreach ($members as &$member) {

        if($member->pl_last_activity > strtotime('-1 week')){
            $member->active = true;
        }else{
            $member->active = false;
        }

        if($member->pl_last_cap > $last_reset){
            $member->has_capped = true;
        }else{
            $member->has_capped = false;
        }

        //set the numeral rank for sorting
        $clan_ranks = ["Owner", "Deputy Owner", "Overseer", "Coordinator", "Organiser", "Admin", "General", "Captain", "Lieutenant", "Sergeant", "Corporal", "Recruit"];
        $member->pl_clan_rank_nr = array_search($member->pl_clan_rank, $clan_ranks);
    }

    //sort on daily exp by default
    usort($members, function($a, $b) use ($sort, $sort_order) {
        if($sort_order == "desc"){
            return $b->$sort <=> $a->$sort;
        }else{
            return $a->$sort <=> $b->$sort;
        }
    });

    //slice members for pagination
    $args['member_slice'] = array_slice($members, $bottom_limit, ($members_per_page-1));

    return $this->view->fetch('dashboard/members.twig', $args);
})->add($owner_only);

$app->map(['GET','POST'], '/dashboard/{name}/settings', function ($request, $response, $args) {

    $clan_id = get_clan_id($args['name']);
    $args['skills'] = get_skills();
    $args['clan'] = get_clan($clan_id);
    $args['members'] = get_clan_members($clan_id);
    $args['weekdays'] = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    if ($request->isPost()) {
        $post = (object)$request->getParams();

            if (isset($_FILES) && !empty($_FILES["logo"]["tmp_name"])) {

                $avatar = new ImageUploader($_FILES['logo']);

                $avatar->width = 128;              // In pixels
                $avatar->sendTo = __DIR__.'/../public/img/clans/';      // Relative path
                $avatar->maxSize = 5;
                $avatar->imageName = clean_underscore($args['clan']->cl_name);

                $upload = $avatar->uploadImage();
                if ($upload->isUploaded) {
                    $args['success'] = "Your clan avatar has been uploaded.";
                } else {
                    $args['error'] = $upload->errorMessage; // Tells what went wrong
                }

            } else if (isset($post->clan_slogan)) {
                update_clan_settings($clan_id, $post->clan_slogan, $post->reset_day, $post->reset_hour, $post->clan_points_visit, $post->clan_points_cap, $post->clan_points_exp);
            }
            $args['clan'] = get_clan($clan_id);
    }

    return $this->view->fetch('dashboard/settings.twig', $args);
})->add($owner_only);

$app->map(['GET','POST'], '/dashboard/{name}/competitions[/{comp_id}]', function ($request, $response, $args) {

    $args['skills'] = get_skills();
    if(isset($args['comp_id'])){
        $args['competition'] = get_competition($args['comp_id']);
    }

    if ($request->isPost()) {
        $post = (object)$request->getParams();

        if(isset($args['comp_id'])){
            $competition_id = $args['comp_id'];
            //update the current comp -> $competition_id

            update_competition($competition_id, $post->comp_name, $_SESSION['player_profile']->pl_clan_id, $post->comp_skill, strtotime($post->comp_start_date), strtotime($post->comp_end_date), $_SESSION['player_profile']->pl_id);
            $args['competition'] = get_competition($args['comp_id']);
        }else{
            //add a new competition ->$post
            add_competition($post->comp_name, $_SESSION['player_profile']->pl_clan_id, $post->comp_skill, strtotime($post->comp_start_date), strtotime($post->comp_end_date), $_SESSION['player_profile']->pl_id);
            $args['competition_message'] = "The competition '$post->comp_name' has been added!";
        }
    }

    $args['upcoming_competitions'] = get_upcoming_competitions();

    return $this->view->fetch('dashboard/competitions.twig', $args);
})->add($owner_only);

/*
    Account/player verification
*/

$app->map(['GET','POST'], '/verify/[{name}]', function ($request, $response, $args){
    $step_index = ['first', 'second', 'third'];
    $args['step'] = 0;
    $is_member = true;
    $verification = false;
    
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == true) {
        $user = get_user($_SESSION['user_id']);
        $player = get_player_by_name($args['name']);
        $player_check = check_confirmed_player($player->pl_id);
        if (!$player_check) {
            
            if ($request->isPost()) {

                $post = (object)$request->getParams();

                //  The post request came from the world type selection form
                if(isset($post->world_form_check)){
                    $args['world_type'] = $post->world_type;
                    $is_member = ($args['world_type'] == 'member' ? true : false);
                    $args['world'] = generate_world($is_member);

                //  The post request came from the verification form
                }else{
                    $step = $post->step;
                    $args['world_type'] = $post->world_type;
                    $verification = check_world($args['name'], $post->world);

                    if ($verification === true) {
                        if ($step < 2) {
                            //pass to the next step
                            $args['step'] = $step + 1;
                           
                        } elseif ($step == 2) {
                            $args['step'] = $step;
                            $args['verified'] = true;
                            confirm_account($user->us_id, $player->pl_id);
                            //set the player profile in the session
                            $user_characters = get_user_characters($user->us_id);
                            if (!$user_characters) {
                                $_SESSION['player_profiles'] = false;
                            } else {
                                $_SESSION['player_profiles'] = [];
                                $_SESSION['player_ids'] = [];
                                foreach($user_characters as $user_character){
                                    $player = get_player($user_character->pl_id);
                                    $clan = get_clan($player->pl_clan_id);
                                    $character_profile = (object)array_merge((array)$player, (array)$clan);
                                    $_SESSION['player_profiles'][] = $character_profile;
                                    $_SESSION['player_ids'][] = $user_character->pl_id;
                                }
                            }
                        }

                    } else {
                        $args['step'] = $step;
                        $args['check_error'] = $verification;
                    }
                    //serve next world
                    $is_member = ($args['world_type'] == 'member' ? true : false);
                    $args['world'] = generate_world($is_member);
                }
                
            } else {
                //first world
                $args['world_type'] = 'member';
                $args['world'] = generate_world($is_member);
            }             
        } else {
            $args['error'] = "You have already linked this Runescape account to you Runelogs account.";
        }

    } else {
        $args['error'] = "You need to be logged in to Runelogs before you can claim a profile.";
    }

    $args['step_text'] = $step_index[$args['step']];
    return $this->view->fetch('verify.twig', $args);
});

/*
    Pin/unpin drops
*/

$app->get('/pin/{player_id}/{drop_id}', function ($request, $response, $args) {

    $player = get_player($args['player_id']);

    if ($_SESSION['logged_in'] == true && in_array($args['player_id'], $_SESSION['player_ids'])){
        pin_drop($args['player_id'], $args['drop_id']);
    }

    return $response->withRedirect('/profile/'.clean($player->pl_name).'/drops');
});

$app->get('/unpin/{player_id}/{drop_id}', function ($request, $response, $args) {

    $player = get_player($args['player_id']);

    if ($_SESSION['logged_in'] == true && in_array($args['player_id'], $_SESSION['player_ids'])){
        unpin_drop($args['player_id'], $args['drop_id']);
    }
    
    return $response->withRedirect('/profile/'.clean($player->pl_name).'/drops');
});

/*
    Activation route for new users
*/

$app->get('/activate/{code}', function ($request, $response, $args) {

    if (isset($args['code'])) {
        $is_user_activated = activate_user($args['code']);

        if ($is_user_activated) {
            return $this->view->fetch('confirm.twig', $args);
        } else {
            return $this->view->fetch('404.twig', $args);
        }
    }
});

/*
    Nightmode<3
*/

$app->get('/toggle-nightmode', function ($request, $response, $args) {

    if (!isset($_SESSION['nightmode'])) {
        $_SESSION['nightmode'] = true;
    } else {
        $_SESSION['nightmode'] = !$_SESSION['nightmode'];
    }
    return $_SESSION['nightmode'];
});

/*
    Login/register routes
*/
    
$app->map(['GET','POST'], '/login', function ($request, $response, $args) use ($app) {

    if ($request->isPost()) {

        $post = (object)$request->getParams();
        $args['form_input']['user_email'] = $post->user_email;

        //validate
        if ($request->getAttribute('has_errors')) { //check for validation errors

              $args['errors'] = $request->getAttribute('errors'); //report them back if there are any

        } else {

            //if not, move on
            if (isset($post->user_email) && isset($post->user_password)) {

                $user = check_user($post->user_email); //  try to look up the client id in the local db

                if (isset($user) && $user != null) {

                    if ($user->us_status == 'active') {
                        if (password_verify($post->user_password, $user->us_password) == true) { //   match the passwords

                            $_SESSION['user_id'] = $user->us_id;
                            $_SESSION['logged_in'] = true; //   login
                            $user_characters = get_user_characters($user->us_id);
                            if (!$user_characters) {
                                $_SESSION['player_profiles'] = false;
                            } else {
                                $_SESSION['player_profiles'] = [];
                                $_SESSION['player_ids'] = [];
                                foreach($user_characters as $user_character){
                                    $player = get_player($user_character->pl_id);
                                    $clan = get_clan($player->pl_clan_id);
                                    $character_profile = (object)array_merge((array)$player, (array)$clan);
                                    $_SESSION['player_profiles'][] = $character_profile;
                                    $_SESSION['player_ids'][] = $user_character->pl_id;
                                }
                            }
                            return $response->withHeader('Location', '/');
                        } else {
                            //wrong password
                            $args['errors']['user_password'][] = "Wrong password";
                        }
                    } else {
                        $args['errors']['user_email'][] = "Inactive account. Check your inbox.";
                    }
                } else {
                    //client not found
                    $args['errors']['user_email'][] = "Unknown email";
                }
            }
        }
    }

    return $this->view->fetch('login.twig', $args);

})->add($container->get('loginValidation'));

$app->get('/unlink/{user_id}', function ($request, $response, $args) use ($app) {
    //user_id
    unlink_account($args['user_id']);
    session_destroy();
    return $response->withHeader('Location', '/login');  
});

$app->get('/logout', function ($request, $response, $args) use ($app) {
    session_destroy();
    return $response->withHeader('Location', '/login');  
});

$app->map(['GET','POST'], '/register', function ($request, $response, $args) use ($app) {

    if ($request->isPost()) {

        $post = (object)$request->getParams();
        $args['form_input']['user_email'] = $post->user_email;

        //validate
        if ($request->getAttribute('has_errors')) {

            $args['errors'] = $request->getAttribute('errors');

        } else {

            $user = check_user($post->user_email); //  try to look up the client id in the local db

            if (isset($user) && $user != null) {
                //client already is in local db
                $args['errors']['user_email'][] = "E-mail is already in use!";           
                
            } else {
                //compare the entered passwords
                if ($post->user_password == $post->user_password_confirm) {

                    //add new user
                    $activation_hash = sha1("runelogs_random_key".$post->user_email);
                    $user_id = add_user($post->user_email, password_hash($post->user_password, PASSWORD_DEFAULT), 'inactive', $activation_hash, 0);
                    
                    //send activation email
                    send_activation_email($post->user_email, $activation_hash);

                    $args['signup_completed'] = true;
                } else {
                    $args['errors']['user_password_confirm'][] = "Passwords don't match.";
                }
            }
        }
    }

    return $this->view->fetch('register.twig', $args);

})->add($container->get('registerValidation'));

/*
    Protected pages
*/

$app->get('/settings', function ($request, $response, $args) {

    return $this->view->fetch('settings.twig', $args);

})->add($protected);

/*
    Api
*/

$app->post('/api/get_time_period_data', function ($request, $response, $args) {
    header("Content-Type: application/json");

    if ($request->isPost()) {

        $post = (object)$request->getParsedBody();

        if($post->period == "custom"){
            $response_data = get_exp_logs($post->player_id, new DateTime($post->start_date), new DateTime($post->end_date));
        }elseif($post->period == "current-week"){
            $response_data = get_exp_logs($post->player_id, new DateTime('-1 week sunday'), new DateTime('today'));
        }elseif($post->period == "last-week"){
            $response_data = get_exp_logs($post->player_id, new DateTime('-2 weeks sunday'), new DateTime('-1 week sunday'));
        }elseif($post->period == "last-month"){
            $response_data = get_exp_logs($post->player_id, new DateTime('first day of this month'), new DateTime('today'));
        }
        
        echo json_encode($response_data);
    }
});

$app->get('/ping', function ($request, $response, $args) {
    header("Content-Type: application/json");
    $status = [];
    $return_value = -1;
    $servers = [
        'master' => '138.68.167.239',
        'slave-one' => '139.59.165.191',
        'slave-two' => '138.68.191.150',
        'slave-three' => '138.68.191.143'
    ];
    $strings = ["online", "offline", "offline"];
    foreach($servers as $server_name => $server_ip){
        exec("ping ".$server_ip." -c1 -w1 2>&1", $output, $return_value);
        $status[$server_name] = $strings[$return_value];
    }
    echo json_encode($status);
});


/*
    RSS
*/

$app->get('/profile/{name}/drops/rss', function ($request, $response, $args){

        $player = get_player_by_name($args['name']);
        $args['name'] = str_replace(' ', '+', $args['name']);
        $args['drops'] = get_drops($player->pl_id);
        return $this->view->fetch('rss/player_drops.rss.xml.twig', $args);
        
})->add($is_rss);

$app->get('/clan/{name}/drops/rss', function ($request, $response, $args){
    
        $clan_id = get_clan_id($args['name']);
        $args['name'] = str_replace(' ', '+', $args['name']);        
        $drops = get_clan_drops($clan_id);//0.013   
        $args['drops'] = $drops;
        return $this->view->fetch('rss/clan_drops.rss.xml.twig', $args);
            
})->add($is_rss);

/*
    Reset route for password reset
*/

$app->get('/reset/{code}', function ($request, $response, $args) {

    if (isset($args['code'])) {
        //check post request -> update password

        //if not post -> serve form
    }
});

/*
    Regular pages
*/

$app->get('/team', function ($request, $response, $args) use ($r) {
    $args['team'] = [
        [
            "name" => "Sander",
            "commits" => 0, 
            "title" => "Project lead",
            "twitter" => "https://twitter.com/dreadnip"
        ],
        [
            "name" => "Kevin",
            "commits" => 0, 
            "title" => "Big boi PHP dev",
            "twitter" => "https://twitter.com/ThatsTheKeikaku"
        ],
        [
            "name" => "Tim",
            "commits" => 0, 
            "title" => "Newbie & JS heretic",
            "twitter" => "https://twitter.com/pandah_rs"
        ],
        [
            "name" => "AoDude",
            "commits" => 0, 
            "title" => "API builder & discord dev",
            "twitter" => "https://twitter.com/aodude1234"
        ],
        [
            "name" => "qtto",
            "commits" => 0, 
            "title" => "Design qt & CSS wizard",
            "twitter" => "https://twitter.com/Qyxo"
        ]
    ];
    $contributors = $r->get_json("https://gitlab.com/api/v4/projects/4173708/repository/contributors?private_token=6toLAPgLgmf3FPZH3SEq");

    foreach($contributors as $contributor){
        foreach($args['team'] as &$team_member){
            if($team_member['name'] == $contributor->name){
                $team_member['commits'] += $contributor->commits;
            }
        }
    }

    usort($args['team'], function($a, $b) {
        return $b['commits'] <=> $a['commits'];
    });

    return $this->view->fetch('team.twig', $args);
});

$app->get('/about', function ($request, $response, $args) {

    return $this->view->fetch('about.twig', $args);
});

$app->get('/news', function ($request, $response, $args) {
    
    return $this->view->fetch('news.twig', $args);
});

$app->get('/status', function ($request, $response, $args) {
    $args['servers'] = get_server_data();
    return $this->view->fetch('status.twig', $args);
});

$app->map(['GET','POST'], '/forgot', function ($request, $response, $args) {

    if ($request->isPost()) {

        $post = (object)$request->getParams();
        $user = get_user_by_email($post->email);
        if(!$user){
            $args['error'] = "We couldn't find a user for that email address.";
        }else{
            reset_password($user);
            $args['success'] = "The reset email has been sent. Check your inbox and spam folder and follow the instructions inside.";
        }   
    }
    
    return $this->view->fetch('forgot.twig', $args);
});

$app->get('/error', function ($request, $response, $args) {

    return $this->view->fetch('error.twig', $args);
});

$app->get('/database-error', function ($request, $response, $args) {

    return $this->view->fetch('database-error.twig', $args);
});

$app->get('/maintenance', function ($request, $response, $args) {

    return $this->view->fetch('maintenance.twig', $args);
});

$app->get('/bye', function ($request, $response, $args) {

    return $this->view->fetch('bye.twig', $args);
});

$app->get('/transparency', function ($request, $response, $args) {

    return $this->view->fetch('transparency.twig', $args);
});

$app->get('/donate/success', function ($request, $response, $args) {

    return $this->view->fetch('donate.twig', $args);
});

$app->get('/support', function ($request, $response, $args) {

    return $this->view->fetch('support.twig', $args);
});

/*
    Debug!
*/

$app->get('/debug', function ($request, $response, $args) use ($r) {
    /*
    $clans = get_clans();
    for ($i=0; $i <= 10; $i++) {
        
        if(isset($clans[$i])){
            $clan = $clans[$i];
            $clan_update_start = microtime(true);
            try {
              roster_update($clan); //new people, leavers, name changes, clan changes
              data_update($clan); //update skill_values, exp_logs, drops & drop_values
              echo "* ".$clan->cl_name." has been updated. [total time: ".round(microtime(true)-$clan_update_start, 2)."s]\r\n";
            } catch (Exception $e) {
                $update_end = microtime(true);
                update_server_end($server, $update_end, false);
                die("!!!! Clan update halted at clan #".$clan->cl_id." with name ".$clan->cl_name.". (".$e->getMessage().")");
            }
        }
    }
    */
    $data = $r->get_bulk_hiscores(['gray+mouser']);
    var_dump($data['gray+mouser']);
});

$app->get('/s', function ($request, $response, $args) use ($app) {
    echo "<pre>";
    print_r($_SESSION);
});

$app->get('/config', function ($request, $response, $args) use ($app) {
    header("Content-Type: application/json");
    $servers = get_server_names();
    /*
    $output = [
        "update_order" => ["master", "slave-one", "slave-two", "slave-three"]
    ];
    */
    echo json_encode($servers);
});

/*
    127.0.0.1
*/

$app->get('/', function ($request, $response, $args) {
    $this->logger->info("Slim-Skeleton '/' route");

    return $this->view->fetch('index.twig', $args);
});