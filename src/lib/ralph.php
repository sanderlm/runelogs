<?php
/*
-------------- Ralph  ---------------
An interface for Runescape web data

    Copyright (c) 2017 @dreadnip
            License: MIT
https://github.com/dreadnip/ralph
*/

namespace Ralph;

require __DIR__.'/../../vendor/autoload.php';
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Pool;
use Psr\Http\Message\ResponseInterface;

Class api
        {
	//	Session/login variables
	private $pug_login = null;
	private $pug_password = null;
	private $logged_in = false;
	private $session_token = null;
    //	Default Guzzle client & base URIs
	private $client = null;
	private $base_runemetrics_url = 'https://apps.runescape.com/runemetrics/';
	private $base_legacy_url = 'http://services.runescape.com/';
	private $base_hiscore_url = 'http://services.runescape.com/m=hiscore/index_lite.ws';
	private $base_im_hiscore_url = 'http://services.runescape.com/m=hiscore_ironman/index_lite.ws';
	private $base_hcim_hiscore_url = 'http://services.runescape.com/m=hiscore_hardcore_ironman/index_lite.ws';
	//	You don't even want to know
	private $skill_list = ["Overall", "Attack", "Defence", "Strength", "Hitpoints", "Ranged", "Prayer", "Magic", "Cooking", "Woodcutting", "Fletching", "Fishing", "Firemaking", "Crafting", "Smithing", "Mining", "Herblore", "Agility", "Thieving", "Slayer", "Farming", "Runecrafting", "Hunter", "Construction", "Summoning", "Dungeoneering", "Divination", "Invention", "Bounty Hunter", "B.H. Rogues", "Dominion Tower", "The Crucible", "Castle Wars games", "B.A. Attackers", "B.A. Defenders", "B.A. Collectors", "B.A. Healers", "Duel Tournament", "Mobilising Armies", "Conquest", "Fist of Guthix", "GG: Athletics", "GG: Resource Race", "WE2: Armadyl Lifetime Contribution", "WE2: Bandos Lifetime Contribution", "WE2: Armadyl PvP kills", "WE2: Bandos PvP kills", "Heist Guard Level", "Heist Robber Level", "CFP: 5 game average", "AF15: Cow Tipping", "AF15: Rats killed after the miniquest"];
	//	Debug output
        	public $request_info = [];

	/*
     *  Constructor
     */
    
	function __construct()
	{
		$this->client =  new Client(['http_errors' => false]);
	}

    /*
     *  base functions
     */

    public function get_raw($url)
    {
    	$response = $this->client->request('GET', $url);
    	if ($response->getStatusCode() == 200) {
    		$body = $response->getBody();
    		return $body->getContents();
    	} else {
    		return false;
    	}
    }

    public function get_json($url, $trim_callback = false)
    {
		//if set_pug has been fired correctly and a login was successful
    	if ($this->logged_in == true) {

			//create a cookie jar with the session token and loggedIn variable for later
    		$cookie_jar = CookieJar::fromArray([
    			'loggedIn' => 'true',
    			'session' => $this->session_token
    		], 'services.runescape.com');

			//set the referer to Runemetrics base url
    		$headers = ['Referer' => 'https://apps.runescape.com/runemetrics/app/friends'];
    	} else {
    		$cookie_jar = null;
    		$headers = null;
    	}

		//perform the request
    	$response = $this->client->request('GET', $url, [
    		'cookies' => $cookie_jar,
			'headers' => $headers //Dont question the jar/headers. They just work, for some fucked up reason
		]);

		//check for a good HTTP code
    	if ($response->getStatusCode() == 200) {

			//get the response content
    		$body = $response->getBody()->getContents();

		    //if this is a callback response, trim it
    		if ($trim_callback == true) {
    			$body = $this->trim_callback($body);
    		}

		    //decode the content
    		$content = json_decode($body);

		    //if there are errors, return them as a property of an object
    		if (isset($content->error)) {
    			return (object)["error" => $content->error];
    		} else {
    			return $content;
    		}
    	} else {
    		return false;
    	}
    }

	/*
     *  Player functions
     */

	/**
	 * Get the Runemetrics profile data for a given player name
	 * @param string $player_name
	 * @return object
	 */
	public function get_profile($player_name)
	{
		return $this->get_json($this->base_runemetrics_url.'profile/profile?user='.$this->norm($player_name).'&activities=20');
	}

	/**
	 * Get the Runescape.com website details for a given player name
	 * @param string $player_name
	 * @return object
	 */
	public function get_details($player_name)
	{
		return $this->get_json($this->base_legacy_url.'m=website-data/playerDetails.ws?membership=true&names=["'.$this->norm($player_name).'"]&callback=angular.callbacks._0', true)[0];
	}

    /**
     * Get the Runescape.com website details for every player in a given list
     * @param array $name_list - names have to be strings
     * @return array - the provided list with details added per player
     */
    public function get_list_details($list)
    {
    	foreach ($list as &$name) {
    		$name = $this->norm($name);
    	}

    	$comb_array = array();
    	if (count($list) > 99) {
    		$chunked = array_chunk($list, 99, false);
    		foreach ($chunked as $sub_array) {
    			$result = $this->get_json($this->base_legacy_url.'m=website-data/playerDetails.ws?membership=true&names='.json_encode($sub_array).'&callback=angular.callbacks._0', true);             
    			$comb_array = array_merge($comb_array, (array)$result);
    		}
    	} else {

    		$comb_array  = $this->get_json($this->base_legacy_url.'m=website-data/playerDetails.ws?membership=true&names='.json_encode($list).'&callback=angular.callbacks._0', true);
    	}
    	return $comb_array;
    }

    /**
     * Get the Runescape.com hiscore data for a given player name
     * @param string $player_name
     * @return array
     */
    public function get_hiscore($player_name)
    {
    	//	Start by trying to get the player's hiscore, if that 404s, try to get an im/hcim with the same name
    	//	I'm aware this looks shady as fuck, but it works so why bother changing it?
    	$response = $this->get_raw($this->base_hiscore_url.'?player='.$this->norm($player_name));
    	if (!$response) {
    		return (object)["error" => 'player doesnt exist'];
    	}

    	//	Convert the dumb csv to an array of objects
    	$list = array();
    	$response = explode("\n", $response);
    	foreach ($response as $key => $row) {
    		if ($key < (count($response)-1)) {
    			$row_items = explode(",", $row);
    			if ($key === 0) {
                    $list[] = (object)array(
                        'name' => $this->skill_list[$key],
                        'rank' => $row_items[0] === "-1" ? 0 : $row_items[0],
                        'level' => $row_items[1],
                        'experience' => $row_items[2] === "-1" ? 0 : $row_items[2]
                    );
                }
    			if ($key > 0 && $key < 28) {
    				$list[] = (object)array(
    					'name' => $this->skill_list[$key],
    					'rank' => $row_items[0] === "-1" ? 0 : $row_items[0],
    					'level' => $row_items[1],
    					'experience' => $row_items[2] === "-1" ? 0 : $row_items[2] . "0"
    				);
    			} else {
    				$list[] = (object)array(
    					'name' => $this->skill_list[$key],
    					'rank' => $row_items[0],
    					'value' => $row_items[1]
    				);
    			}
    		}
    	}
    	return $list;
    }

    function check_im($player_name)
    {
        $response = $this->get_raw('http://services.runescape.com/m=hiscore_ironman/index_lite.ws?player='.$this->norm($player_name));
        if (!$response) {
            return false;
        } else {
            return true;
        }
    }

    function check_hcim($player_name)
    {
        $response = $this->get_raw('http://services.runescape.com/m=hiscore_hardcore_ironman/index_lite.ws?player='.$this->norm($player_name));
        if (!$response) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get the quest data (started/completed) for a given player name
     * @param string $player_name
     * @return array
     */
    public function get_quests($player_name)
    {
    	return $this->get_json($this->base_runemetrics_url.'quests?user='.$this->norm($player_name))->quests; 
    }

    /**
     * Get the player avatar URL for a given player name
     * @param string $player_name
     * @return string
     */
    public function get_avatar($player_name)
    {
    	return $this->base_legacy_url.'m=avatar-rs/'.$this->norm($player_name).'/chat.png';
    }

    /*
     *  Clan functions
     * 
     *	To do:
     *		- more multi functions (see rue)
     */

    public function get_clan_list($clan_name, $light = false)
    {
    	$raw_list = $this->get_raw($this->base_legacy_url.'m=clan-hiscores/members_lite.ws?clanName='.$this->norm($clan_name));
    	$check = explode(",", $raw_list, 2);
    	if ($check[0] == 'Clanmate') {
    		$clan_list = array();
    		$raw_list = explode("\n", $raw_list);
    		foreach ($raw_list as $key => $row) {
    			if ($key != 0 && $key <= (count($raw_list)-2)) {
    				$row_item = explode(",", $row);
    				$name = htmlentities(utf8_encode($row_item[0]));
    				if ($light == true) {
    					array_push($clan_list, str_replace('&nbsp;', ' ', $name));
    				} else {
    					$clan_list[] = (object)array(
    						'name' => str_replace('&nbsp;', ' ', $name),
    						'rank' => $row_item[1],
    						'clan_xp' => $row_item[2],
    						'clan_kills' => $row_item[3]
    					);
    				}
    			}
    		}
    		return $clan_list;
    	} else {
    		return (object)['error' => 'CLAN NOT FOUND'];
    	}
    }

    public function get_indexed_clan_list($clan_name, $light = false)
    {
        $raw_list = $this->get_raw($this->base_legacy_url.'m=clan-hiscores/members_lite.ws?clanName='.$this->norm($clan_name));
        $check = explode(",", $raw_list, 2);
        if ($check[0] == 'Clanmate') {
            $clan_list = array();
            $raw_list = explode("\n", $raw_list);
            foreach ($raw_list as $key => $row) {
                if ($key != 0 && $key <= (count($raw_list)-2)) {
                    $row_item = explode(",", $row);
                    $name = str_replace('&nbsp;', ' ', htmlentities(utf8_encode($row_item[0])));
                    if ($light == true) {
                        array_push($clan_list, $name);
                    } else {
                        $clan_list[clean($name)] = (object)array(
                            'name' => $name,
                            'clan_rank' => $row_item[1],
                            'clan_xp' => $row_item[2],
                            'clan_kills' => $row_item[3]
                        );
                    }
                }
            }
            return $clan_list;
        } else {
            return (object)['error' => 'CLAN NOT FOUND'];
        }
    }

    public function get_bulk_hiscores($player_names)
    {
        $client = new Client(['base_uri' => 'services.runescape.com/']);

        $output = array();

        $requests = function ($player_names) {
            foreach ($player_names as $member_index => $member) {
                yield new Request('GET', $this->base_hiscore_url . '?player=' . $this->norm($member));
            }
        };

        $pool = new Pool($client, $requests($player_names), [
            'concurrency' => 10,
            'fulfilled' => function (ResponseInterface $response, $index) use (&$output, $player_names)
            {

                if ($response->getStatusCode() == 200) {

                    $content = $response->getBody()->getContents();
                    $list = array();
                    $content = explode("\n", $content);
                    foreach ($content as $key => $row) {
                        if ($key < (count($content)-1)) {
                            $row_items = explode(",", $row);
                            if ($key === 0) {
                                $list[$this->skill_list[$key]] = (object)array(
                                    'name' => $this->skill_list[$key],
                                    'rank' => $row_items[0] === "-1" ? 0 : $row_items[0],
                                    'level' => $row_items[1],
                                    'xp' => $row_items[2] === "-1" ? 0 : $row_items[2]
                                );
                            }
                            if ($key > 0 && $key < 28) {
                                $list[$this->skill_list[$key]] = (object)array(
                                    'name' => $this->skill_list[$key],
                                    'rank' => $row_items[0] === "-1" ? 0 : $row_items[0],
                                    'level' => $row_items[1],
                                    'xp' => $row_items[2] === "-1" ? 0 : $row_items[2] . "0"
                                );
                            }
                        }
                    }

                    $output_object['skillvalues'] = $list;
                    $output[$player_names[$index]] = (object)$output_object;

                }

            },
            'rejected' => function (RequestException $reason, $index) use (&$output, $player_names)
            {
                if ($reason->getCode() == 404) {
                    $output[$player_names[$index]] = (object)array('_code' => 404, '_error' => 'Player not found.');
                }
                //$s['fail']++;
            }
        ]);

        $promise = $pool->promise();
        $promise->wait(); //push the button

        return $output;
    }


    //todo: try async (with delay)
	//todo: compare sync speed (7.1 on droplet) with async
	//first test: multi_curl > guzzle (0.5 vs 2.2s)
	//larger batch: 107 vs 105 (kinda equal)
    public function get_clan_profiles($clan_name)
    {
    	$clan_list = $this->get_clan_list($clan_name, true);

    	$output = [];

    	$client = new Client(['base_uri' => 'https://apps.runescape.com/']);

    	$requests = function ($clan_list)
    	{
    		foreach ($clan_list as $member_index => $member) {
    			yield new Request('GET', $this->base_runemetrics_url.'/profile/profile?user='.$this->norm($member).'&activities=20');
    		}
    	};

    	$pool = new Pool($client, $requests($clan_list), [
    		'concurrency' => 25,
    		'fulfilled' => function ($response, $index) use (&$output)
    		{

    			if ($response->getStatusCode() == 200) {

    				$output[] = json_decode($response->getBody()->getContents());
    			}

    		},
    		'rejected' => function ($reason, $index)
    		{

		       //$s['fail']++;
    		}
    	]);

    	$promise = $pool->promise();
		$promise->wait(); //push the button

		return $output;
	}

    public function get_list_profiles($list)
    {
        $output = [];

        $client = new Client(['base_uri' => 'https://apps.runescape.com/']);

        $requests = function ($list)
        {
            foreach ($list as $player_name => $player_id) {
                yield new Request('GET', $this->base_runemetrics_url.'/profile/profile?user='.$this->norm($player_name).'&activities=20');
            }
        };

        $pool = new Pool($client, $requests($list), [
            'concurrency' => 25,
            'fulfilled' => function ($response, $index) use ($list, &$output)
            {

                if ($response->getStatusCode() == 200) {

                    $profile = json_decode($response->getBody()->getContents());
                    if(!isset($profile->error)){
                        $profile->id = $list[$this->norm($profile->name)];
                    }
                    $output[] = $profile;    
                }

            },
            'rejected' => function ($reason, $index)
            {

               //$s['fail']++;
            }
        ]);

        $promise = $pool->promise();
        $promise->wait(); //push the button

        return $output;
    }

    public function test_pool($list)
    {
        $output = [];
        $output['200'] = 0;
        $output['profiles'] = [];
        $output['404'] = 0;

        $client = new Client(['base_uri' => 'https://apps.runescape.com/']);

        $requests = function ($list)
        {
            foreach ($list as $player_name => $player_id) {
                yield new Request('GET', $this->base_runemetrics_url.'/profile/profile?user='.$this->norm($player_name).'&activities=20');
            }
        };

        $pool = new Pool($client, $requests($list), [
            'concurrency' => 25,
            'fulfilled' => function ($response, $index) use ($list, &$output)
            {

                if ($response->getStatusCode() == 200) {

                    $profile = json_decode($response->getBody()->getContents());
                    $output['200']++;
                    $output['profiles'][] = $profile;  
                }

            },
            'rejected' => function ($reason, $index)
            {

               $output['404']++;
            }
        ]);

        $promise = $pool->promise();
        $promise->wait(); //push the button

        return $output;
    }

	public function get_clan_details($clan_name)
	{
		$list = $this->get_clan_list($clan_name, true);
		return $this->get_list_details($list);
	}

	/**
	 * Get the Runescape.com hiscore data for an entire clan
	 * @param string $clan_name
	 * @return array of objects. Each object had the 'name' and 'skills' properties
	 */
	public function get_clan_hiscores($clan_name)
	{
		$clan_list = $this->get_clan_list($clan_name, true);

		$output = [];

		$client = new Client(['base_uri' => 'services.runescape.com/']);

		$requests = function ($clan_list)
		{
			foreach ($clan_list as $member_index => $member) {
				yield new Request('GET', $this->base_hiscore_url.'?player='.$this->norm($member));
			}
		};

		$pool = new Pool($client, $requests($clan_list), [
			'concurrency' => 10,
			'fulfilled' => function ($response, $index) use (&$output, $clan_list)
			{

				if ($response->getStatusCode() == 200) {

					$content = $response->getBody()->getContents();
					$list = array();
					$content = explode("\n", $content);
					foreach ($content as $key => $row) {
						if ($key < (count($content)-1)) {
							$row_items = explode(",", $row);
							if ($key < 28) {
								$list[] = (object)array(
									'name' => $this->skill_list[$key],
									'rank' => $row_items[0],
									'level' => $row_items[1],
									'experience' => $row_items[2]
								);
							} else {
								$list[] = (object)array(
									'name' => $this->skill_list[$key],
									'rank' => $row_items[0],
									'value' => $row_items[1]
								);
							}
						}
					}

					$output_object['name'] = $clan_list[$index];
					$output_object['skills'] = $list;
					$output[] = (object)$output_object;

				}

			},
			'rejected' => function ($reason, $index)
			{

		       //$s['fail']++;
			}
		]);

		$promise = $pool->promise();
		$promise->wait(); //push the button

		return $output;
	}

    /**
     * Scrapes the seasonals page.
     */
    public function scrape_seasonals()
    {
        $seasonal_url = "http://services.runescape.com/m=temp-hiscores/?filter=1&entries=500";

        $dom = parse_page($seasonal_url);
        $xpath = new \DOMXPath($dom);
        $seasonal_names = $xpath->query('//div[@class="tempHSOverviewBoxTitleContainer"]/h4/text()');
        $winners = $xpath->query('//div[@class="tempHSOverviewBoxBottomSection tempHSBoxBottomPast"]/p[2]/text()');
        $timestamps = $xpath->query('//div[@class="tempHSOverviewBoxTopSection"]/p/text()');

        $result = array();

        foreach ($seasonal_names as $index => $seasonal) {
            $name = trim($seasonal->nodeValue);

            $winner = htmlentities($winners[$index]->nodeValue, null, 'utf-8');
            $winner = str_replace('&nbsp;', ' ',  $winner);

            $timestamp = htmlentities($timestamps[$index]->nodeValue, null, 'utf-8');
            $timestamp = str_replace('&nbsp;', ' ',  $timestamp);
            $timestamp = trim($timestamp);
            $timestamp = strtotime($timestamp);

            $result[] = (object)array(
                'name' => $name,
                'winner' => $winner,
                'timestamp' => $timestamp,
            );
        }

        return $result;
    }

	/*
	 *  Token functions
	 */

	public function set_pug($email, $password)
	{
		$this->pug_login = $email;
		$this->pug_password = $password;

        //try to perform a 'log in' post request
		if ($this->perform_login()) {
        	$this->logged_in = true; //this means $this->session_token is now a valid token
            return true;
        }else{
            return false;
        }
    }

    private function perform_login()
    {
    	$base_login_url = "https://secure.runescape.com/m=weblogin/login.ws";

    	$response = $this->client->request('POST', $base_login_url, [
    		'form_params' => [
    			'username' => $this->pug_login,
    			'password' => $this->pug_password,
    			'mod' => urlencode("www"),
    			'ssl' => urlencode("0"),
    			'dest' => urlencode("community")   
    		],
    		'headers' => [
    			'Host' => 'secure.runescape.com',
    			'Referer' => 'https://secure.runescape.com/m=weblogin/login.ws'
    		]
    	]);

    	//got redirected after login -> successful login
    	if ($response->hasHeader('Set-Cookie')) {
            
    		$this->set_session_token($response->hasHeader('Set-Cookie'));

    		return true;
    	} else {
    		return false;
    	};
    }

	/*
	 *  Helper functions
	 */

	public function norm($string)
	{
		return str_replace(' ', '+', htmlentities(utf8_encode(strtolower($string))));
	}

	private function trim_callback($response_string)
	{
		$response_string = substr($response_string, 21);
		$response_string = substr($response_string, 0, -3);
		return $response_string;
	}

    private function set_session_token($session_cookie)
    {
        $session_index_start = strpos($session_cookie[0], 'session=') + 8;
        $session_length = strpos($session_cookie[0], ';', $session_index_start) - $session_index_start;
        $this->session_token = substr($session_cookie[0], $session_index_start, $session_length);
    }
}