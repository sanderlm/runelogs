<?php
// Application middleware

//Enables the Whoops error pages
$app->add(new \Zeuxisoo\Whoops\Provider\Slim\WhoopsMiddleware);

//Provides a global 'route' parameter to all templates with the current url pattern (used to make the page title)
$app->add(function ($request, $response, $next) {
    $route = $request->getAttribute('route');
    if (!empty($route)) {
        $arguments = $route->getArguments();
        $pattern_pieces = explode('/', $route->getPattern());
        $arguments['route'] = ucfirst($pattern_pieces[1]);
        $route->setArguments($arguments);
    }
    $response = $next($request, $response);
    return $response;
});

//Basic login protection. Will redirect to the login page instead of serving the asked response if the user is not logged in.
//Usage: see the /settings route
$protected = function ($request, $response, $next) {
    $response = $next($request, $response);
    if(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == true){
        return $response;
    }else{
        return $response->withStatus(302)->withHeader('Location', '/login');
    }
};

$owner_only = function ($request, $response, $next) {
    $route = $request->getAttribute('route');
    if (!empty($route)) {
        $arguments = $route->getArguments();
        $pattern_pieces = explode('/', $route->getPattern());
        $clan_name = end($pattern_pieces);
    }
    $response = $next($request, $response);
    if(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == true && isset($_SESSION['player_profiles']) && $_SESSION['player_profiles'] != false){
        foreach($_SESSION['player_profiles'] as $player_profile){
            if($player_profile->cl_name == $clan_name && $player_profile->pl_clan_rank = "Owner"){
                return $response;
            }
        }
        return $response->withStatus(302)->withHeader('Location', '/error');
    }else{
        return $response->withStatus(302)->withHeader('Location', '/login');
    }
};

$xhr_only = function ($request, $response, $next) {
    $response = $next($request, $response);
    if ($request->isXhr()) {
        return $response;
    }else{
        return $response->withStatus(302)->withHeader('Location', '/');
    }
};

//Passing the nightmode session variable to each template, so header.twig can read it
$app->add(function ($request, $response, $next) {
    if(isset($_SESSION['nightmode'])){
        $route = $request->getAttribute('route');
        if (!empty($route)) {
            $arguments = $route->getArguments();
            $arguments['nightmode'] = $_SESSION['nightmode'];
            $route->setArguments($arguments);
        }
    }
    $response = $next($request, $response);
    return $response;
});

//Check for a database connection and show a custom db error page if the db is down
$app->add(function ($request, $response, $next) {
    $route = $request->getAttribute('route');
    if (!empty($route)) {
        $route_name = $route->getPattern();
        if(return_handler() instanceof PDOException && $route_name != '/database-error'){
            return $response->withStatus(302)->withHeader('Location', '/database-error');
        }
    }
    $response = $next($request, $response);
    return $response;
});

$is_rss = function ($request, $response, $next) {
    return $response->withHeader('Content-Type', 'application/xml');
};

$app->add(function ($request, $response, $next) {
    $route = $request->getAttribute('route');
    if (!empty($route)) {
        $route_name = $route->getPattern();
        if($this->get('settings')['maintenance'] === true && $route_name != '/maintenance'){
            return $response->withStatus(302)->withHeader('Location', '/maintenance');
        }
    }
    $response = $next($request, $response);
    return $response;
});

$app->add(function ($request, $response, $next) {
    $route = $request->getAttribute('route');
    if (!empty($route)) {
        $route_name = $route->getPattern();
        if($this->get('settings')['shutdown'] === true && $route_name != '/bye'){
            return $response->withStatus(302)->withHeader('Location', '/bye');
        }
    }
    $response = $next($request, $response);
    return $response;
});