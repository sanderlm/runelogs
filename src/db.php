<?php

use Symfony\Component\Yaml\Yaml;

/* ============================================
            Database functions
============================================ */

function return_handler()
{
    $host = getParameter('database.host');
    $name = getParameter('database.name');
    $user = getParameter('database.user');
    $pass = getParameter('database.pass');

    $dsn = "mysql:host=$host;dbname=$name;charset=utf8";

    $opts = [
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
    ];

    try {
      return new PDO($dsn, $user, $pass, $opts);
    }
    catch(PDOException $e) {
        return $e;
        exit;
    }
}

//  Clans

function get_clans()
{
    $pdo = return_handler();
    $sql = "SELECT * FROM clans";
    $result = $pdo->query($sql)->fetchAll();
    $pdo = null;
    return $result;
}

function get_clan($clan_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM clans WHERE cl_id = :clan_id");
    $stmt->bindParam(':clan_id', $clan_id);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    if (isset($result[0])) {
        return $result[0];
    } else {
        return false;
    } 
}

function get_clan_id($clan_name)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT cl_id FROM clans WHERE cl_name = :clan_name");
    $stmt->bindParam(':clan_name', $clan_name);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
    $pdo = null;
    if (isset($result[0])) {
        return $result[0];
    } else {
        return false;
    }
}

function add_clan($new_clan_name)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("INSERT INTO clans VALUES (null, :new_clan_name, :clan_slogan, :clan_reset_day, :clan_reset_hour, :clan_visit_points, :clan_cap_points, :clan_exp_points)");
    $stmt->bindParam(':new_clan_name', $new_clan_name);
    $stmt->bindValue(':clan_slogan', '');
    $stmt->bindValue(':clan_reset_day', 0);
    $stmt->bindValue(':clan_reset_hour', 0);
    $stmt->bindValue(':clan_visit_points', 0);
    $stmt->bindValue(':clan_cap_points', 0);
    $stmt->bindValue(':clan_exp_points', 0);
    $stmt->execute();

    $new_id = $pdo->lastInsertId(); //user id just created
    $pdo = null;
    return $new_id;
}

function delete_clan($clan_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("DELETE FROM clans WHERE cl_id = :clan_id");
    $stmt->bindParam(':clan_id', $clan_id);
    $stmt->execute();

    $stmt = $pdo->prepare("UPDATE players SET pl_clan_id = 0 WHERE pl_clan_id = :clan_id");
    $stmt->bindParam(':clan_id', $clan_id);
    $stmt->execute();
    $pdo = null;
}

function get_clan_names($clan_name)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT pl_name FROM players INNER JOIN clans ON clans.cl_id = players.pl_clan_id WHERE cl_name = :clan_name");
    $stmt->bindParam(':clan_name', $clan_name);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
    $pdo = null;
    return $result;  
}

function get_clan_members($clan_id)
{
    //SELECT * FROM players INNER JOIN clans ON clans.cl_id = players.pl_clan_id WHERE cl_id = :clan_id
    $pdo = return_handler();
    //$stmt = $pdo->prepare("SELECT distinct players.*, exp_logs.ex_created FROM `players` INNER JOIN clans ON players.pl_clan_id = clans.cl_id INNER JOIN exp_logs ON exp_logs.ex_pl_id = players.pl_id  WHERE clans.cl_id = :clan_id and ex_value - ex_original_value > 0 ORDER BY (CASE WHEN (pl_clan_rank = 'Owner') then 1 WHEN (pl_clan_rank = 'Deputy Owner') then 2 WHEN (pl_clan_rank = 'Overseer') THEN 3 WHEN (pl_clan_rank = 'Coordinator') THEN 4 WHEN (pl_clan_rank = 'Organiser') THEN 5 WHEN (pl_clan_rank = 'Admin') THEN 6 WHEN (pl_clan_rank = 'General') THEN 7 WHEN (pl_clan_rank = 'Captain') THEN 8 WHEN (pl_clan_rank = 'Lieutenant') THEN 9 WHEN (pl_clan_rank = 'Sergeant') THEN 10 WHEN (pl_clan_rank = 'Corporal') THEN 11 WHEN (pl_clan_rank = 'Recruit') THEN 12 else 13 END) ASC");
    $stmt = $pdo->prepare("SELECT * FROM players WHERE pl_clan_id = :clan_id ORDER BY (CASE WHEN (pl_clan_rank = 'Owner') then 1 WHEN (pl_clan_rank = 'Deputy Owner') then 2 WHEN (pl_clan_rank = 'Overseer') THEN 3 WHEN (pl_clan_rank = 'Coordinator') THEN 4 WHEN (pl_clan_rank = 'Organiser') THEN 5 WHEN (pl_clan_rank = 'Admin') THEN 6 WHEN (pl_clan_rank = 'General') THEN 7 WHEN (pl_clan_rank = 'Captain') THEN 8 WHEN (pl_clan_rank = 'Lieutenant') THEN 9 WHEN (pl_clan_rank = 'Sergeant') THEN 10 WHEN (pl_clan_rank = 'Corporal') THEN 11 WHEN (pl_clan_rank = 'Recruit') THEN 12 else 13 END) ASC");
    $stmt->bindParam(':clan_id', $clan_id);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    return $result;    
}

function generate_pleb_list()
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT pl_name FROM players WHERE pl_clan_id = :clan_id ");
    $stmt->bindValue(':clan_id', 0);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
    $pdo = null;
    return $result;    
}

function leave_clan($player_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("UPDATE players SET pl_clan_id = 0 WHERE pl_id = :player_id");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->execute();
    $pdo = null;
}

function update_clan_settings($clan_id, $clan_slogan, $clan_reset_day, $clan_reset_hour, $clan_points_visit, $clan_points_cap, $clan_points_exp)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("UPDATE clans SET cl_slogan = :cl_slogan, cl_reset_day = :cl_reset_day, cl_reset_hour = :cl_reset_hour, cl_visit_points = :cl_visit_points, cl_cap_points = :cl_cap_points, cl_exp_points = :cl_exp_points WHERE cl_id = :clan_id");
    $stmt->bindParam(':clan_id', $clan_id);
    $stmt->bindParam(':cl_slogan', $clan_slogan);
    $stmt->bindParam(':cl_reset_day', $clan_reset_day);
    $stmt->bindParam(':cl_reset_hour', $clan_reset_hour);
    $stmt->bindParam(':cl_visit_points', $clan_points_visit);
    $stmt->bindParam(':cl_cap_points', $clan_points_cap);
    $stmt->bindParam(':cl_exp_points', $clan_points_exp);
    $stmt->execute();
    $pdo = null;
}

//  Players

function get_player($player_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM players WHERE pl_id = :player_id");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    if (isset($result[0])) {
        return $result[0];
    } else {
        return false;
    }  
}

function get_player_by_name($player_name)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM players WHERE pl_name = :player_name");
    $stmt->bindParam(':player_name', $player_name);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    if (isset($result[0])) {
        return $result[0];
    } else {
        return false;
    } 
}

function get_player_id($player_name)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT pl_id FROM players WHERE pl_name = :player_name");
    $stmt->bindParam(':player_name', $player_name);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
    $pdo = null;
    if (isset($result[0])) {
        return $result[0];
    } else {
        return false;
    }
}

function get_player_name_by_last_name($player_name)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT pl_name FROM players WHERE pl_last_name = :player_name");
    $stmt->bindParam(':player_name', $player_name);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
    $pdo = null;
    if (isset($result[0])) {
        return $result[0];
    } else {
        return false;
    }
}

function add_player($name, $clan_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("INSERT INTO players VALUES (null, :pl_name, :pl_last_name, :pl_type, :pl_clan_id, :pl_clan_rank, :pl_clan_exp, :pl_clan_kills, :pl_last_cap, :pl_last_visit, :pl_cap_count, :pl_clan_points, :pl_total_skill, :pl_total_quests, :pl_total_exp, :pl_total_rank, :pl_combat_level, :pl_last_updated, :pl_is_private, :pl_last_activity)");
    $stmt->bindParam(':pl_name', $name);
    $stmt->bindValue(':pl_last_name', '');
    $stmt->bindValue(':pl_type', 0);
    $stmt->bindParam(':pl_clan_id', $clan_id);
    $stmt->bindValue(':pl_clan_rank', 0);
    $stmt->bindValue(':pl_clan_exp', 0);
    $stmt->bindValue(':pl_clan_kills', 0);
    $stmt->bindValue(':pl_last_cap', 0);
    $stmt->bindValue(':pl_last_visit', 0);
    $stmt->bindValue(':pl_cap_count', 0);
    $stmt->bindValue(':pl_clan_points', 0);
    $stmt->bindValue(':pl_total_skill', 0);
    $stmt->bindValue(':pl_total_quests', 0);
    $stmt->bindValue(':pl_total_exp', 0);
    $stmt->bindValue(':pl_total_rank', 0);
    $stmt->bindValue(':pl_combat_level', 0);
    $stmt->bindValue(':pl_last_updated', 0);
    $stmt->bindValue(':pl_is_private', 0);
    $stmt->bindValue(':pl_last_activity', 0);
    $stmt->execute();
    $new_id = $pdo->lastInsertId();
    $pdo = null;
    return $new_id;
}

function add_players($players_to_add, $clan_id) {

    $pdo = return_handler();
    $pdo->beginTransaction ();

    $stmt = $pdo->prepare("INSERT INTO players VALUES (null, :pl_name, :pl_last_name, :pl_type, :pl_clan_id, :pl_clan_rank, :pl_clan_exp, :pl_clan_kills, :pl_last_cap, :pl_last_visit, :pl_cap_count, :pl_clan_points, :pl_total_skill, :pl_total_quests, :pl_total_exp, :pl_total_rank, :pl_combat_level, :pl_last_updated, :pl_is_private, :pl_last_activity)");
    foreach ($players_to_add as $player) {
        $stmt->bindParam(':pl_name', $player);
        $stmt->bindValue(':pl_last_name', '');
        $stmt->bindValue(':pl_type', 0);
        $stmt->bindParam(':pl_clan_id', $clan_id);
        $stmt->bindValue(':pl_clan_rank', 0);
        $stmt->bindValue(':pl_clan_exp', 0);
        $stmt->bindValue(':pl_clan_kills', 0);
        $stmt->bindValue(':pl_last_cap', 0);
        $stmt->bindValue(':pl_last_visit', 0);
        $stmt->bindValue(':pl_cap_count', 0);
        $stmt->bindValue(':pl_clan_points', 0);
        $stmt->bindValue(':pl_total_skill', 0);
        $stmt->bindValue(':pl_total_quests', 0);
        $stmt->bindValue(':pl_total_exp', 0);
        $stmt->bindValue(':pl_total_rank', 0);
        $stmt->bindValue(':pl_combat_level', 0);
        $stmt->bindValue(':pl_last_updated', 0);
        $stmt->bindValue(':pl_is_private', 0);
        $stmt->bindValue(':pl_last_activity', 0);
        $stmt->execute();
    }

    $pdo->commit();
    $pdo = null;
}

function update_player($player_id, $clan_rank, $clan_exp, $clan_kills, $total_skill, $total_quests, $total_exp, $total_rank, $combat_level, $timestamp, $profile_status)
{
    $total_rank = intval(str_replace(',', '', $total_rank));
    $pdo = return_handler();
    $stmt = $pdo->prepare("UPDATE players SET pl_clan_rank = :pl_clan_rank, pl_clan_exp = :pl_clan_exp, pl_clan_kills = :pl_clan_kills, pl_total_skill = :pl_total_skill, pl_total_quests = :pl_total_quests, pl_total_exp = :pl_total_exp, pl_total_rank = :pl_total_rank, pl_combat_level = :pl_combat_level, pl_last_updated = :pl_last_updated, pl_is_private = :pl_is_private WHERE pl_id = :player_id");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->bindParam(':pl_clan_rank', $clan_rank);
    $stmt->bindParam(':pl_clan_exp', $clan_exp);
    $stmt->bindParam(':pl_clan_kills', $clan_kills);
    $stmt->bindParam(':pl_total_skill', $total_skill);
    $stmt->bindParam(':pl_total_quests', $total_quests);
    $stmt->bindParam(':pl_total_exp', $total_exp);
    $stmt->bindParam(':pl_total_rank', $total_rank);
    $stmt->bindParam(':pl_combat_level', $combat_level);
    $stmt->bindParam(':pl_last_updated', $timestamp);
    $stmt->bindParam(':pl_is_private', $profile_status);
    $stmt->execute();
    $pdo = null;
}

function update_player_name($player_id, $new_name, $old_name)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("UPDATE players SET pl_name = :new_name, pl_last_name = :old_name WHERE pl_id = :player_id");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->bindParam(':new_name', $new_name);
    $stmt->bindParam(':old_name', $old_name);
    $stmt->execute();
    $pdo = null;
}

function update_player_clan($player_id, $new_clan_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("UPDATE players SET pl_clan_id = :new_clan_id WHERE pl_id = :player_id");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->bindParam(':new_clan_id', $new_clan_id);
    $stmt->execute();
    $pdo = null;
}

function update_player_activity($player_id, $activity_timestamp)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("UPDATE players SET pl_last_activity = :activity_timestamp WHERE pl_id = :player_id");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->bindParam(':activity_timestamp', $activity_timestamp);
    $stmt->execute();
    $pdo = null;
}

function cap($player_id, $timestamp)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("UPDATE players SET pl_last_cap = :cap_timestamp, pl_cap_count = pl_cap_count + 1 WHERE pl_id = :player_id");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->bindParam(':cap_timestamp', $timestamp);
    $stmt->execute();
    $pdo = null;
}

function set_visit($player_id, $timestamp)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("UPDATE players SET pl_last_visit = :visit_timestamp WHERE pl_id = :player_id");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->bindParam(':visit_timestamp', $timestamp);
    $stmt->execute();
    $pdo = null;
}

function set_type($player_id, $type)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("UPDATE players SET pl_type = :type WHERE pl_id = :player_id");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->bindParam(':type', $type);
    $stmt->execute();
    $pdo = null;
}

//  Skills & skill_values

function get_skills()
{
    $pdo = return_handler();
    $sql = "SELECT * FROM skills";
    $result = $pdo->query($sql)->fetchAll();
    $pdo = null;
    return $result;
}

function get_skill_value($player_id, $skill_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM exp_logs WHERE ex_pl_id = :player_id AND ex_sk_id = :skill_id");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->bindParam(':skill_id', $skill_id);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    if (isset($result[0])) {
        return $result[0];
    } else {
        return false;
    }
}

function get_skill_values($player_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM `exp_logs` WHERE `ex_pl_id` = :player_id order by `ex_sk_id` asc");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    return $result;    
}

function get_clan_skill_values($clan)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT exp_logs.ex_pl_id, exp_logs.ex_sk_id, exp_logs.ex_value, exp_logs.ex_level, exp_logs.ex_rank FROM `exp_logs` inner join players on exp_logs.ex_pl_id = players.pl_id inner join clans on players.pl_clan_id = clans.cl_id WHERE `cl_id` = :clan_id order by exp_logs.ex_pl_id, exp_logs.ex_sk_id asc");
    $stmt->bindParam(':clan_id', $clan);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_GROUP);
    $pdo = null;
    return $result;    
}

function update_skills($clan_update_array)
{
    $pdo = return_handler();
    $pdo->beginTransaction ();
    $start = microtime(true);

    foreach ($clan_update_array as $player_index => $player_update_array) {

        //Archive old exp logs + add new exp_log record for today
        $ex_archive_stmt = $pdo->prepare("INSERT INTO old_exp_logs SELECT * FROM exp_logs WHERE ex_pl_id = :pl_id AND ex_sk_id = :sk_id");
        $ex_delete_stmt = $pdo->prepare("DELETE FROM exp_logs WHERE ex_pl_id = :pl_id AND ex_sk_id = :sk_id");
        $ex_add_stmt = $pdo->prepare("INSERT INTO exp_logs VALUES (null, :pl_id, :sk_id, :value, :level, :rank, :original_value, :original_level, :original_rank, :timestamp)");
        foreach ($player_update_array->exp_logs_to_add as $ex_add_object) {
            //move to _old_
            $ex_archive_stmt->bindValue(':pl_id', (int)$ex_add_object->player_id);
            $ex_archive_stmt->bindValue(':sk_id', (int)$ex_add_object->skill->id);
            $ex_archive_stmt->execute();

            //delete from current
            $ex_delete_stmt->bindValue(':pl_id', (int)$ex_add_object->player_id);
            $ex_delete_stmt->bindValue(':sk_id', (int)$ex_add_object->skill->id);
            $ex_delete_stmt->execute();

            //add new record
            $ex_add_stmt->bindValue(':pl_id', (int)$ex_add_object->player_id);
            $ex_add_stmt->bindValue(':sk_id', (int)$ex_add_object->skill->id);
            $ex_add_stmt->bindValue(':value', (double)$ex_add_object->skill->xp);
            $ex_add_stmt->bindValue(':level', (int)$ex_add_object->skill->level);
            $ex_add_stmt->bindValue(':rank', (int)$ex_add_object->skill->rank);
            $ex_add_stmt->bindValue(':original_value',(double) $ex_add_object->skill->xp);
            $ex_add_stmt->bindValue(':original_level', (int)$ex_add_object->skill->level);
            $ex_add_stmt->bindValue(':original_rank', (int)$ex_add_object->skill->rank);
            $ex_add_stmt->bindValue(':timestamp', (int)$ex_add_object->timestamp);
            $ex_add_stmt->execute();
        }

        //Update exp logs - ONLY UPDATE THE LAST VALUE
        $ex_update_stmt = $pdo->prepare("UPDATE exp_logs SET ex_value = :value, ex_level = :level, ex_rank = :rank WHERE ex_pl_id = :player_id AND ex_sk_id = :skill_id order by ex_created desc limit 1");
        foreach ($player_update_array->exp_logs_to_update as $ex_update_object) {
            $ex_update_stmt->bindValue(':player_id', (int)$ex_update_object->player_id);
            $ex_update_stmt->bindValue(':skill_id', (int)$ex_update_object->skill->id);
            $ex_update_stmt->bindValue(':value', (double)$ex_update_object->skill->xp);
            $ex_update_stmt->bindValue(':level', (int)$ex_update_object->skill->level);
            $ex_update_stmt->bindValue(':rank', (int)$ex_update_object->skill->rank);
            $ex_update_stmt->execute();
        }

        //Add achievements (always add)
        $ac_add_stmt = $pdo->prepare("INSERT INTO achievement_values VALUES (null, :player_id, :skill_id, :achievement_type_id, :achievement_timestamp)");
        foreach ($player_update_array->achievements_to_add as $ac_add_object) {
            $ac_add_stmt->bindValue(':player_id', (int)$ac_add_object->player_id);
            $ac_add_stmt->bindValue(':skill_id', (int)$ac_add_object->skill->id);
            $ac_add_stmt->bindValue(':achievement_type_id', (int)$ac_add_object->achievement_id);
            $ac_add_stmt->bindValue(':achievement_timestamp', time());
            $ac_add_stmt->execute();
        }

    }

    try {
        $pdo->commit();
        $pdo = null;
    } catch (Exception $e) {
        $pdo->rollBack();
        $pdo = null;
        return $e;
    }
}

//  Users

function get_user($user_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM users WHERE us_id = :user_id");
    $stmt->bindParam(':user_id', $user_id);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    if (isset($result[0])) {
        return $result[0];
    } else {
        return false;
    }  
}

function get_user_by_email($email)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM users WHERE us_email = :email");
    $stmt->bindParam(':email', $email);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    if (isset($result[0])) {
        return $result[0];
    } else {
        return false;
    } 
}

function check_user($user_email)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM users WHERE us_email = :user_email");
    $stmt->bindParam(':user_email', $user_email);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    if (isset($result[0])) {
        return $result[0];
    } else {
        return false;
    } 
}

function get_user_characters($user_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM users_characters WHERE us_id = :user_id");
    $stmt->bindParam(':user_id', $user_id);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    if (isset($result[0])) {
        return $result;
    } else {
        return false;
    } 
}

function activate_user($code)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM users WHERE us_activation_hash = :code");
    $stmt->bindParam(':code', $code);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if (isset($result[0]['us_id'])) {
        $stmt = $pdo->prepare("UPDATE users SET us_status = 'active', us_activation_hash = '' WHERE us_activation_hash = :code");
        $stmt->bindParam(':code', $code);
        $stmt->execute();
        $pdo = null;
        return true;
    } else {
        $pdo = null;
        return false;
    }
}

function add_user($email, $password, $status, $hash, $confirmed_player)
{
    $pdo = return_handler();;
    $stmt = $pdo->prepare("INSERT INTO users VALUES (null, :password, :email, :status, :hash, :confirmed_player)");
    $stmt->bindParam(':password', $password);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':status', $status);
    $stmt->bindParam(':hash', $hash);
    $stmt->bindParam(':confirmed_player', $confirmed_player);
    $stmt->execute();
    $new_id = $pdo->lastInsertId(); //user id just created
    $pdo = null;
    return $new_id;
}

function confirm_account($user_id, $player_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("INSERT INTO users_characters VALUES (:user_id, :player_id)");
    $stmt->bindParam(':user_id', $user_id);
    $stmt->bindParam(':player_id', $player_id);
    $stmt->execute();
    $pdo = null;
}

function unlink_account($user_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("DELETE FROM `users_characters` WHERE us_id = :user_id");
    $stmt->bindParam(':user_id', $user_id);
    $stmt->execute();
    $pdo = null;
}

function check_confirmed_player($player_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM `users_characters` WHERE `pl_id` = :player_id");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $pdo = null;
    if (isset($result[0])) {
        return true;
    } else {
        return false;
    }
}

// Drops

/* Possibly an improvement... Mixed results on production?
 * SELECT `dv_pl_id`, `dr_title`, `latest_activity` FROM `players` INNER JOIN ( SELECT `dv_pl_id`, `dv_dr_id`, MAX(`dv_timestamp`) AS `latest_activity` FROM `drop_values` GROUP BY `dv_pl_id` ) AS `latest_drop_table` ON `pl_id` = `dv_pl_id` INNER JOIN `drops` ON `dr_id` = `dv_dr_id` WHERE `pl_clan_id` = :clan_id
 */
function get_clan_last_drops($clan_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT DISTINCT t1.dv_pl_id, drops.dr_title, t1.dv_timestamp FROM drop_values t1 INNER JOIN drops ON t1.dv_dr_id = drops.dr_id INNER JOIN players on t1.dv_pl_id = players.pl_id INNER JOIN clans on players.pl_clan_id = clans.cl_id WHERE clans.cl_id = :clan_id AND t1.dv_timestamp = (SELECT MAX(t2.dv_timestamp) FROM drop_values t2 WHERE t2.dv_pl_id = t1.dv_pl_id)");
    $stmt->bindParam(':clan_id', $clan_id);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_GROUP);
    $pdo = null;
    return $result;
}

function get_last_drop($player_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM drop_values INNER JOIN drops ON drop_values.dv_dr_id = drops.dr_id WHERE dv_pl_id = :player_id ORDER BY dv_timestamp DESC LIMIT 1");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    if (isset($result[0])) {
        return $result;
    } else {
        return false;
    }
}

function get_drops($player_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM drop_values INNER JOIN drops ON drop_values.dv_dr_id = drops.dr_id WHERE dv_pl_id = :player_id AND dv_pinned = 0 ORDER BY dv_timestamp DESC");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $pdo = null;
    return $result; 
}

function find_drop($drop_text)
{
    $item_name = '';
    $new_filter = ["I found some", "I found a pair of", "I found a set of", "I found the", "I found an", "I found a", "I found"];
    foreach ($new_filter as $filter) {
        $filter_length = strlen($filter);
        if (strpos($drop_text, $filter) !== false) {
            $item_name = substr($drop_text, strpos($drop_text, $filter)+$filter_length);
            //if the drop has ", the" in it, it's a pet drop
            if (strpos($item_name, ", the") !== false) {
                $item_name = substr($item_name, 0, strpos($item_name, ", the"));    
            }
            //some drops have periods at the end
            if (strpos($item_name, ".") !== false) {
                $item_name = substr($item_name, 0, strpos($item_name, "."));    
            }
            break;
        }
    }
    $item_name = trim($item_name);
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT `it_id`  FROM `items` WHERE `it_name` LIKE :item_name");
    $stmt->bindParam(':item_name', $item_name);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
    $pdo = null;
    if (isset($result[0])) {
        return $result[0];
    } else {
        return 0;
    }
}

function get_pinned_drops($player_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM drop_values INNER JOIN drops ON drop_values.dv_dr_id = drops.dr_id WHERE dv_pl_id = :player_id AND dv_pinned = 1 ORDER BY dv_timestamp DESC");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $pdo = null;
    return $result;  
}

function get_clan_drops($clan_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT players.pl_name, drops.dr_img_id, drops.dr_title, drop_values.dv_timestamp FROM drop_values INNER JOIN drops ON drop_values.dv_dr_id = drops.dr_id INNER JOIN players ON drop_values.dv_pl_id = players.pl_id INNER JOIN clans ON players.pl_clan_id = clans.cl_id WHERE clans.cl_id = :clan_id ORDER BY dv_timestamp DESC");
    $stmt->bindParam(':clan_id', $clan_id);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $pdo = null;
    return $result;    
}

function add_drops($drops_to_add)
{
    $pdo = return_handler();
    foreach ($drops_to_add as $player) {
        if (!empty($player)) {
            foreach ($player as $drop_obj) {
                $stmt = $pdo->prepare("SELECT * FROM `drops` WHERE `dr_title` = :dr_title");
                $stmt->bindParam(':dr_title', $drop_obj->drop->text);
                $stmt->execute();
                $result = $stmt->fetchAll();
                if (isset($result[0])) {
                    //drop already exists
                    $drop_id = $result[0]->dr_id;
                } else {
                    //new drop
                    $drop_img_id = find_drop($drop_obj->drop->text);
                    $dr_add_stmt = $pdo->prepare("INSERT INTO drops VALUES (null, :dr_title, :dr_hidden, :dr_img_id)");
                    $dr_add_stmt->bindParam(':dr_title', $drop_obj->drop->text);
                    $dr_add_stmt->bindValue(':dr_hidden', 0);
                    $dr_add_stmt->bindParam(':dr_img_id', $drop_img_id);
                    $dr_add_stmt->execute();
                    $drop_id = $pdo->lastInsertId(); //drop id just created
                }
                //add new drop value
                $dv_add_stmt = $pdo->prepare("INSERT INTO drop_values VALUES (null, :player_id, :drop_id, :timestamp, 0)");
                $dv_add_stmt->bindParam(':player_id', $drop_obj->player_id);
                $dv_add_stmt->bindParam(':drop_id', $drop_id);
                $dv_add_stmt->bindValue(':timestamp', strtotime($drop_obj->drop->date));
                $dv_add_stmt->execute();
            }
        }
    }
    $pdo = null;
}

function pin_drop($player_id, $drop_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT count(`dv_pinned`) AS count FROM `drop_values` WHERE `dv_pinned` = 1 and `dv_pl_id` = :player_id");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->execute();
    $result = $stmt->fetchAll();

    if ($result[0]->count >= 3) {
        $pdo = null;
        return false;
    } else {
        $stmt = $pdo->prepare("UPDATE drop_values SET dv_pinned = 1 WHERE dv_id = :drop_id");
        $stmt->bindParam(':drop_id', $drop_id);
        $stmt->execute();
        $pdo = null;
    }
};

function unpin_drop($player_id, $drop_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("UPDATE drop_values SET dv_pinned = 0 WHERE dv_id = :drop_id");
    $stmt->bindParam(':drop_id', $drop_id);
    $stmt->execute();
    $pdo = null;
};

//  Exp logging

function get_clan_last_exp_logs($clan_id){
    $pdo = return_handler();
    $now = time();
    $begin_of_today = strtotime("midnight", $now);
    $end_of_today = strtotime("tomorrow", $begin_of_today) - 1;
    $stmt = $pdo->prepare("SELECT DISTINCT exp_logs.ex_pl_id, exp_logs.ex_sk_id, exp_logs.ex_value, exp_logs.ex_level, exp_logs.ex_rank FROM `exp_logs` INNER JOIN players on exp_logs.ex_pl_id = players.pl_id INNER JOIN clans on players.pl_clan_id = clans.cl_id WHERE clans.`cl_id` = :clan_id AND `ex_created` >= :ts_start AND `ex_created` < :ts_end order by exp_logs.ex_pl_id, `ex_sk_id` asc");
    $stmt->bindParam(':clan_id', $clan_id);
    $stmt->bindParam(':ts_start', $begin_of_today);
    $stmt->bindParam(':ts_end', $end_of_today);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_GROUP);
    $pdo = null;
    return $result; 
}

function get_last_exp_logs($player_id)
{
    $pdo = return_handler();
    $now = time();
    $begin_of_today = strtotime("midnight", $now);
    $end_of_today = strtotime("tomorrow", $begin_of_today) - 1;
    $stmt = $pdo->prepare("SELECT DISTINCT * FROM `exp_logs` WHERE `ex_pl_id` = :player_id AND `ex_created` >= :ts_start AND `ex_created` < :ts_end order by `ex_sk_id` asc");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->bindParam(':ts_start', $begin_of_today);
    $stmt->bindParam(':ts_end', $end_of_today);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $pdo = null;
    return $result; 
}

function get_clan_activity($clan_id)
{
    $pdo = return_handler();
    $now = time();

    $stmt = $pdo->prepare("SELECT players.pl_name, GREATEST(exp_logs.ex_created, old_exp_logs.ex_created) as lastactivity 
        FROM exp_logs LEFT JOIN old_exp_logs on exp_logs.ex_pl_id = old_exp_logs.ex_id
        INNER JOIN players ON exp_logs.ex_pl_id = players.pl_id
        INNER JOIN clans ON players.pl_clan_id = clans.cl_id
        WHERE clans.cl_id = :clan_id and (exp_logs.ex_value - exp_logs.ex_original_value > 0
        OR old_exp_logs.ex_value - old_exp_logs.ex_original_value > 0)
        GROUP BY players.pl_name");
    $stmt->bindParam(':clan_id', $clan_id);
    $stmt->execute();
    $result = $stmt->fetchAll();

    $pdo = null;
    $output = [];
    foreach ($result as $row) {
        $output[$row->pl_name] = $row->lastactivity;
    }
    return $output; 
}

//$start_date & $end_date have to be DateTime objects
function get_exp_logs($player_id, $start_date, $end_date = null)
{
    //  Generate the current date and check if we need to pull the 'alive' records
    $pdo = return_handler();
    $now = time();
    $start_today = strtotime("midnight", $now);

    //  Turn the start date into a timestamp
    $start_date_ts = $start_date->getTimestamp();

    //  Check if we're working with a single day or a period
    if (is_null($end_date)) {
        //  Just 1 day, generate the day begin/end
        $begin = strtotime("midnight", $start_date_ts);
        $end   = strtotime("tomorrow", $begin) - 1;

        if($begin >= $start_today){
            //  the start timestamp is from today -> pull from alive records
            $stmt = $pdo->prepare("SELECT * FROM `exp_logs` WHERE `ex_pl_id` = :player_id ORDER BY ex_sk_id desc");
            $stmt->bindParam(':player_id', $player_id);
            $stmt->execute();
            $result = $stmt->fetchAll(); // exp_logs only has todays logs so this will always return 26 logs

        }else if($end < $start_today){
            //  pull from archive table
            $stmt = $pdo->prepare("SELECT * FROM `old_exp_logs` WHERE `ex_pl_id` = :player_id AND ex_created >= :begin AND ex_created <= :end ORDER BY ex_sk_id desc");
            $stmt->bindParam(':player_id', $player_id);
            $stmt->bindParam(':begin', $begin);
            $stmt->bindParam(':end', $end);
            $stmt->execute();
            $result = $stmt->fetchAll(); // this will return the 26 logs for that player for that day
        }

        //  Format
        $output = [];
        foreach ($result as $exp_log) {
            $obj = (object)[];
            $obj->experience = $exp_log->ex_value - $exp_log->ex_original_value;
            $obj->level = $exp_log->ex_level - $exp_log->ex_original_level;
            $obj->rank = $exp_log->ex_original_rank - $exp_log->ex_rank;
            $output[$exp_log->ex_sk_id] = $obj;
        }
    } else {
        //  Period, generate both begin/end ts for both dates
        $start_date_begin = strtotime("midnight", $start_date_ts);
        $start_date_end   = strtotime("tomorrow", $start_date_begin) - 1;

        $end_date_ts = $end_date->getTimestamp();
        $end_date_begin = strtotime("midnight", $end_date_ts);
        $end_date_end   = strtotime("tomorrow", $end_date_begin) - 1;

        if($start_date_begin < $start_today && $end_date_end > $start_today){
            //  The period overlaps with active/archive table records
            // -> first records are from archive, end records are from daily table
            
            //start
            $start_stmt = $pdo->prepare("SELECT * FROM `old_exp_logs` WHERE `ex_pl_id` = :player_id AND ex_created >= :begin AND ex_created <= :end ORDER BY ex_sk_id desc");
            $start_stmt->bindParam(':player_id', $player_id);
            $start_stmt->bindParam(':begin', $start_date_begin);
            $start_stmt->bindParam(':end', $start_date_end);
            $start_stmt->execute();
            $start_result = $start_stmt->fetchAll();

            //end
            $end_stmt = $pdo->prepare("SELECT * FROM `exp_logs` WHERE `ex_pl_id` = :player_id ORDER BY ex_sk_id desc");
            $end_stmt->bindParam(':player_id', $player_id);
            $end_stmt->execute();
            $end_result = $end_stmt->fetchAll();

        }else if($end_date_end < $start_today){
            //  Only pull from archive table
            
            //start
            $start_stmt = $pdo->prepare("SELECT * FROM `old_exp_logs` WHERE `ex_pl_id` = :player_id AND ex_created >= :begin AND ex_created <= :end ORDER BY ex_sk_id desc");
            $start_stmt->bindParam(':player_id', $player_id);
            $start_stmt->bindParam(':begin', $start_date_begin);
            $start_stmt->bindParam(':end', $start_date_end);
            $start_stmt->execute();
            $start_result = $start_stmt->fetchAll();

            //end
            $end_stmt = $pdo->prepare("SELECT * FROM `old_exp_logs` WHERE `ex_pl_id` = :player_id AND ex_created >= :begin AND ex_created <= :end ORDER BY ex_sk_id desc");
            $end_stmt->bindParam(':player_id', $player_id);
            $end_stmt->bindParam(':begin', $end_date_begin);
            $end_stmt->bindParam(':end', $end_date_end);
            $end_stmt->execute();
            $end_result = $end_stmt->fetchAll();
        }

        $output = [];
        foreach ($start_result as $exp_log_index => $exp_log) {
            $obj = (object)[];
            $obj->experience = $end_result[$exp_log_index]->ex_value - $exp_log->ex_original_value;
            $obj->level = $end_result[$exp_log_index]->ex_level - $exp_log->ex_original_level;
            $obj->rank = $end_result[$exp_log_index]->ex_original_rank - $exp_log->ex_rank;
            $output[$exp_log->ex_sk_id] = $obj;
        }
    }

    $pdo = null;
    return $output; 
}

function get_clan_exp_logs($clan_id, $start_date, $end_date = null)
{
    $begin_ts = $start_date->getTimestamp();
    if (!is_null($end_date)) {
        $end_ts = $end_date->getTimestamp();
        $begin = strtotime("midnight", $begin_ts);
        $end   = strtotime("tomorrow", $end_ts) - 1;
    } else {
        $begin = strtotime("midnight", $begin_ts);
        $end   = strtotime("tomorrow", $begin) - 1;
    }

    //generate the current date and check if we need to pull the 'alive' records
    $now = time();
    $start_today = strtotime("midnight", $now);
    $pdo = return_handler();

    if($begin >= $start_today){
        //the start timestamp is from today -> only pull the alive records
        $stmt = $pdo->prepare("SELECT ex_pl_id, exp_logs.* FROM `exp_logs` INNER JOIN players ON exp_logs.ex_pl_id = players.pl_id INNER JOIN clans ON players.pl_clan_id = clans.cl_id WHERE clans.cl_id = :clan_id ORDER BY exp_logs.ex_created desc");
        $stmt->bindParam(':clan_id', $clan_id);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_GROUP);

    }else if($begin < $start_today && $end > $start_today){
        //the end timestamp is from today but the start ts is from earlier -> pull both the alive and stale records

        //alive
        $alive_stmt = $pdo->prepare("SELECT * FROM `exp_logs` INNER JOIN players ON exp_logs.ex_pl_id = players.pl_id INNER JOIN clans ON players.pl_clan_id = clans.cl_id WHERE clans.cl_id = :clan_id ORDER BY exp_logs.ex_created desc");
        $alive_stmt->bindParam(':clan_id', $clan_id);
        $alive_stmt->execute();
        $alive_result = $alive_stmt->fetchAll(PDO::FETCH_GROUP);

        //old
        $old_stmt = $pdo->prepare("SELECT DISTINCT players.pl_id, exp_logs.* FROM `exp_logs` INNER JOIN players ON exp_logs.ex_pl_id = players.pl_id INNER JOIN clans ON players.pl_clan_id = clans.cl_id WHERE clans.cl_id = :clan_id AND ex_created >= :begin AND ex_created <= :end order by ex_created desc");
        $old_stmt->bindParam(':clan_id', $clan_id);
        $old_stmt->bindParam(':begin', $begin);
        $old_stmt->bindParam(':end', $end);
        $old_stmt->execute();
        $old_result = $old_stmt->fetchAll(PDO::FETCH_GROUP);

        //merge
        $result = array_merge($alive_result, $old_result);
    }else if($end < $start_today){
        //only pull stale records
        $stmt = $pdo->prepare("SELECT DISTINCT players.pl_id, exp_logs.* FROM `exp_logs` INNER JOIN players ON exp_logs.ex_pl_id = players.pl_id INNER JOIN clans ON players.pl_clan_id = clans.cl_id WHERE clans.cl_id = :clan_id AND ex_created >= :begin AND ex_created <= :end order by ex_created desc");
        $stmt->bindParam(':clan_id', $clan_id);
        $stmt->bindParam(':begin', $begin);
        $stmt->bindParam(':end', $end);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_GROUP);
    }
    
    $output = [];
    foreach ($result as $player_id => $player_array) {
        //$nested_player_array = $player_array;
        $clean_obj = (object)[
            "experience" => 0,
            "level" => 0,
            "rank" => 0
        ];
        foreach ($player_array as $k =>$exp_log) { //if it's a 1 day period, just loop the exp_logs and add them in a nice, readable object
            $clean_obj->experience += ($exp_log->ex_value - $exp_log->ex_original_value);
            $clean_obj->level += ($exp_log->ex_level - $exp_log->ex_original_level);
            $clean_obj->rank += ($exp_log->ex_original_rank - $exp_log->ex_rank);
        }
        $output[$player_id] = $clean_obj;
    }

    $pdo = null;
    return $output; 
}

//  Competitions

function get_competition($competition_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM competitions WHERE cp_id = :comp_id");
    $stmt->bindParam(':comp_id', $competition_id);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    if (isset($result[0])) {
        return $result[0];
    } else {
        return false;
    } 
}

function get_clan_competitions($clan_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM competitions INNER JOIN players on cp_created_by = players.pl_id INNER JOIN clans on players.pl_clan_id = clans.cl_id WHERE clans.cl_id = :clan_id");
    $stmt->bindParam(':clan_id', $clan_id);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    return $result;
}

function get_upcoming_competitions()
{
    $now = time();
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM competitions WHERE cp_start_date > :start_date");
    $stmt->bindParam(':start_date', $now);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    return $result;
}

function add_competition($name, $clan_id, $skill_id, $start_date, $end_date, $created_by)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("INSERT INTO competitions VALUES (NULL, :name, :clan_id, :skill_id, :start_date, :end_date, :created_by)");
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':clan_id', $clan_id);
    $stmt->bindParam(':skill_id', $skill_id);
    $stmt->bindParam(':start_date', $start_date);
    $stmt->bindParam(':end_date', $end_date);
    $stmt->bindParam(':created_by', $created_by);
    $stmt->execute();
    $new_id = $pdo->lastInsertId(); //user id just created
    $pdo = null;
    return $new_id;
}

function update_competition($comp_id, $name, $clan_id, $skill_id, $start_date, $end_date, $created_by)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("UPDATE `competitions` SET `cp_name` = :name, `cp_cl_id` = :clan_id, `cp_sk_id` = :skill_id, `cp_start_date` = :start_date, `cp_end_date` = :end_date, `cp_created_by` = :created_by WHERE `competitions`.`cp_id` = :comp_id");
    $stmt->bindParam(':comp_id', $comp_id);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':clan_id', $clan_id);
    $stmt->bindParam(':skill_id', $skill_id);
    $stmt->bindParam(':start_date', $start_date);
    $stmt->bindParam(':end_date', $end_date);
    $stmt->bindParam(':created_by', $created_by);
    $stmt->execute();
    $pdo = null;
}

function get_seasonals()
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM seasonals");
    $stmt->execute();
    $result = $stmt->fetchAll();
    $pdo = null;
    return $result; 
}

function add_seasonal($seasonal, $title)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("INSERT INTO seasonals VALUES (NULL, :seasonal, :title)");
    $stmt->bindParam(':seasonal', $seasonal);
    $stmt->bindParam(':title', $title);
    $stmt->execute();
    $id = $pdo->lastInsertId();
    $pdo = null;
    return $id;
}

function add_player_seasonal($player, $seasonal, $timestamp)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("INSERT INTO player_seasonals VALUES (NULL, :player_id, :seasonal_id, :timestamp)");
    $stmt->bindParam(':player_id', $player);
    $stmt->bindParam(':seasonal_id', $seasonal);
    $stmt->bindParam(':timestamp', $timestamp);
    $stmt->execute();
    $id = $pdo->lastInsertId();
    $pdo = null;
    return $id;
}

function get_player_seasonals($player)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM player_seasonals INNER JOIN seasonals ON ps_se_id = se_id WHERE ps_pl_id = :player_id");
    $stmt->bindParam(':player_id', $player);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $pdo = null;
    return $result;
}

function get_achievement_types()
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM achievements");
    $stmt->execute();
    $result = $stmt->fetchAll();
    $pdo = null;
    return $result; 
}


function get_clan_achievements($clan_id)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT players.pl_id, players.pl_name, achievements.at_message, achievement_values.ac_sk_id FROM achievement_values INNER JOIN players on players.pl_id = achievement_values.ac_pl_id INNER JOIN clans on players.pl_clan_id = clans.cl_id INNER JOIN achievements on achievement_values.ac_at_id = achievements.at_id WHERE clans.cl_id = :clan_id");
    $stmt->bindParam(':clan_id', $clan_id);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $pdo = null;
    return $result; 
}

// Titles

function add_title($title = "RUNELOGS_PLACEHOLDER", $colour = "#363636")
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("INSERT INTO titles VALUES (NULL, :title, :colour)");
    $stmt->bindParam(':title', $title);
    $stmt->bindParam(':colour', $colour);
    $stmt->execute();
    $id = $pdo->lastInsertId();
    $pdo = null;
    return $id;
}

function add_player_title($player, $title)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("INSERT INTO player_titles VALUES (NULL, :player, :title)");
    $stmt->bindParam(':player', $player);
    $stmt->bindParam(':title', $title);
    $stmt->execute();
    $id = $pdo->lastInsertId();
    $pdo = null;
    return $id;
}

function get_seasonal_player_titles($player_id = null)
{
    $pdo = return_handler();

    if ($player_id === null) {
        $stmt = $pdo->prepare("SELECT * FROM player_titles INNER JOIN seasonals ON pt_ti_id = se_ti_id");
    } else {
        $stmt = $pdo->prepare("SELECT * FROM player_titles INNER JOIN seasonals ON pt_ti_id = se_ti_id WHERE pt_pl_id = :player_id");
        $stmt->bindParam(':player_id', $player_id);
    }

    $stmt->execute();
    $result = $stmt->fetchAll();
    $pdo = null;
    return $result;
}

function get_player_available_titles($player_id, $excluded = "RUNELOGS_PLACEHOLDER")
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT pt_id, ti_colour, ti_name FROM titles INNER JOIN player_titles ON pt_ti_id = ti_id WHERE pt_pl_id = :player_id AND ti_name != :excluded");
    $stmt->bindParam(':player_id', $player_id);
    $stmt->bindParam(':excluded', $excluded);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $pdo = null;
    return $result;
}

function get_seasonals_with_placeholder_title()
{
    return "TODO";
}

//  Server & update statistics

function get_server_data()
{
    $pdo = return_handler();
    $sql = "SELECT * FROM servers";
    $result = $pdo->query($sql)->fetchAll();
    $pdo = null;
    return $result;
}

function get_server_names()
{
    $pdo = return_handler();
    $sql = "SELECT sr_name FROM servers";
    $result = $pdo->query($sql)->fetchAll(PDO::FETCH_COLUMN, 0);
    $pdo = null;
    return $result;
}

function get_server($server_name)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("SELECT * FROM servers WHERE sr_name = :server_name");
    $stmt->bindParam(':server_name', $server_name);
    $stmt->execute();

    $result = $stmt->fetchAll();
    $pdo = null;
    if (isset($result[0])) {
        return $result[0];
    } else {
        return false;
    } 
}

function update_server_start($server_id, $update_start_timestamp)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("UPDATE `servers` SET `sr_last_update_start` = :update_start_timestamp, `sr_last_update_status` = :update_status, `sr_last_update_end` = :update_end_timestamp WHERE `servers`.`sr_id` = :server_id");
    $stmt->bindParam(':server_id', $server_id);
    $stmt->bindParam(':update_start_timestamp', $update_start_timestamp);
    $stmt->bindValue(':update_end_timestamp', 0);
    $stmt->bindValue(':update_status', 'busy');
    $stmt->execute();
    $pdo = null;
}

function update_server_end($server, $update_end_timestamp, $update_status)
{
    $pdo = return_handler();
    $stmt = $pdo->prepare("UPDATE `servers` SET `sr_last_update_end` = :update_end_timestamp, `sr_last_update_status` = :update_status, `sr_update_count` = :update_count WHERE `servers`.`sr_id` = :server_id");
    $stmt->bindParam(':server_id', $server->sr_id);
    $stmt->bindParam(':update_end_timestamp', $update_end_timestamp);
    if($update_status == true){
        $stmt->bindValue(':update_status', 'success');
        $update_count = $server->sr_update_count + 1;    
    }else{
        $stmt->bindValue(':update_status', 'fail');
        $update_count = 0;  
    }
    $stmt->bindParam(':update_count', $update_count);
    $stmt->execute();
    $pdo = null;
}

//  Leaderboards

function get_clan_leaderboard_data()
{
    //get all clans

    //loop over all clans

    //
}
