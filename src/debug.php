<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../conf.d/configparser.php';
require __DIR__ . '/../src/db.php';
require __DIR__ . '/../src/controller.php';
require __DIR__ . '/../src/lib/ralph.php';
/*
This file can be used to debug from the cli/cron
*/
$clans = get_clans();
for ($i=0; $i <= 10; $i++) {
    
    if(isset($clans[$i])){
        $clan = $clans[$i];
        $clan_update_start = microtime(true);
        try {
          roster_update($clan); //new people, leavers, name changes, clan changes
          data_update($clan); //update skill_values, exp_logs, drops & drop_values
          echo "* ".$clan->cl_name." has been updated. [total time: ".round(microtime(true)-$clan_update_start, 2)."s]\r\n";
        } catch (Exception $e) {
            $update_end = microtime(true);
            update_server_end($server, $update_end, false);
            die("!!!! Clan update halted at clan #".$clan->cl_id." with name ".$clan->cl_name.". (".$e->getMessage().")");
        }
    }
}