<?php

use Masterminds\HTML5;

/* ============================================
        Update functions (used by cron)
            - clan_update
            - player_update
============================================ */

function recheck_plebs()
{
    $r = new \Ralph\api();
    $plebs = generate_pleb_list();
    
    $pleb_list_with_details = $r->get_list_details($plebs);

    foreach($pleb_list_with_details as $pleb){
        if($pleb != false && isset($pleb->clan)){
            $clan_id = get_clan_id($pleb->clan);
            if(!$clan_id){
                $new_clan_id = add_clan($pleb->clan);
                add_player($pleb->name, $new_clan_id);
            }else{
                update_player_clan(get_player_id($pleb->name), $clan_id);
            }
        }
    }
}

function player_update($player_id)
{
    $r = new \Ralph\api();
    $skills = get_skills();
    $achievement_types = get_achievement_types();
    $player = get_player($player_id);
    $clan = get_clan($player->pl_clan_id);
    
    if ($player->pl_last_updated == '0' || $player->pl_last_updated < strtotime("-5 minutes")) {
        $profile = $r->get_profile($player->pl_name);

        $update_container = [];
        $update_container['skills'] = [];
        $update_container['drops'] = [];
        if (!isset($profile->error)) {
            if (!$clan) {
                $profile->clan_rank = 0;
                $profile->clan_xp = 0;
                $profile->clan_kills = 0;
            } else {
                $fresh_list = $r->get_indexed_clan_list($clan->cl_name);
                $clan_object = $fresh_list[clean($profile->name)];
                $profile = (object)array_merge((array)$profile, (array)$clan_object);
            }
            $profile->id = $player_id;

            $clan_last_exp_logs[$profile->id] = get_last_exp_logs($profile->id);
            $clan_last_drops[$profile->id] = get_last_drop($profile->id);

            $update_container['skills'][] = prep_skills($skills, $achievement_types, $clan_last_exp_logs, $profile);
            $update_container['drops'][] = prep_drops($profile, $clan, $clan_last_drops);

            update_skills($update_container['skills']);
            add_drops($update_container['drops']);

            if($profile->totalxp > $local_data[$profile->id]->pl_total_exp || !empty($player_drops)){
                update_player_activity($profile->id, time());
            }
            update_player($profile->id, $profile->clan_rank, $profile->clan_xp, $profile->clan_kills, $profile->totalskill, $profile->questscomplete, $profile->totalxp, $profile->rank, $profile->combatlevel, time(), 0);
        }
    }
}

function roster_update($clan)
{
    $r = new \Ralph\api();
    $start = microtime(true);

    /*
    The roster update function uses a player list from the database & a fresh one from Jagex
    to determine differences between these 2 lists. The so called 'leftovers' are either new people,
    leavers or players who changed their name and/or clan.
    */

    $current_list = get_clan_names($clan->cl_name);
    $new_list = $r->get_clan_list($clan->cl_name, true);

    if(is_array($new_list)){

        $old_leftovers = array_diff($current_list, $new_list);
        $new_leftovers = array_diff($new_list, $current_list);
        echo "===============================================================================================\r\n";
        echo '>'.$clan->cl_name.' | Members(db): '.count($current_list).' | Members(Jagex): '.count($new_list).' | Old leftovers: '.count($old_leftovers).' | New leftovers: '.count($new_leftovers)."\n";  
        echo "* Starting roster update.. ";

        //Old leftovers => name changes, leavers & clans changers
        foreach ($old_leftovers as $old_leftover) {

            //try to find a name change (high match %) -> unset the guy in new_leftovers and update his record
            $has_been_matched = false;
            foreach ($new_leftovers as &$new_leftover) {
                if (match($old_leftover, $new_leftover)) {
                    //update the old player's record with the new info
                    update_player_name(get_player_id($old_leftover), $new_leftover, $old_leftover);
                    $has_been_matched = true;
                    unset($new_leftover);
                    break;
                }
            }

            if (!$has_been_matched) {//if no match is found -> he left the clan

                //get the player that left's details
                $leaver_details = $r->get_details($old_leftover);
                $leaver_id = get_player_id($old_leftover);
                
                if (isset($leaver_details->clan)) { //check if he's in a clan
                    $existing_clan = get_clan_id($leaver_details->clan);//try to look up the clan name in the db
                    if (!$existing_clan) { //the leaver has a new clan, which is not in our database
                        $new_clan = add_clan($leaver_details->clan); //add it
                        update_player_clan($leaver_id, $new_clan); //set his clan to it
                    } else {
                        update_player_clan($leaver_id, $existing_clan);
                    }
                } else {
                    leave_clan($leaver_id);
                }
            }
        }

        $players_to_add = [];
        foreach ($new_leftovers as $new_leftover) { //most of these are new people
            
            $player_id = get_player_id($new_leftover);
            if (!$player_id) {
                $players_to_add[] = $new_leftover;
            } else {
                //the player is already in the database, so update his clan to this one
                update_player_clan($player_id, $clan->cl_id);
            }
        }
        add_players($players_to_add, $clan->cl_id);

        echo "[done - ".round(microtime(true)-$start, 2)."s]\r\n";
    }else{
        //get_clan_list returned an error (CLAN NOT FOUND) -> delete clan
        delete_clan($clan->cl_id);
    }
}

function data_update($clan)
{
    $r = new \Ralph\api();
    $start = microtime(true);
    $skills = get_skills();
    $achievement_types = get_achievement_types();
    $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

    $local_list = get_clan_members($clan->cl_id);
    $fresh_list = $r->get_indexed_clan_list($clan->cl_name);

    if(is_array($fresh_list)){
        $clan_last_exp_logs = get_clan_last_exp_logs($clan->cl_id);
        $clan_last_drops = get_clan_last_drops($clan->cl_id);

        $to_update = []; //determine if a player needs an update (either new or stale)
        $local_data = [];
        foreach ($local_list as $local_player) {
            if ($local_player->pl_last_updated == '0' || $local_player->pl_last_updated < strtotime("-30 minutes")) {
                $to_update[clean($local_player->pl_name)] = $local_player->pl_id;
                $local_data[$local_player->pl_id] = $local_player;
            }
        }

        echo "* Getting Runemetrics profiles for ".count($to_update)." members.. ";
        $profiles = $r->get_list_profiles($to_update);
        echo "[done - ".round(microtime(true)-$start, 2)."s]\r\n";
        $loop_start = microtime(true);

        $private_profiles = 0;
        $update_container = [];
        $update_container['skills'] = [];
        $update_container['drops'] = [];
        echo "* Prepping player update arrays.. ";
        foreach ($profiles as &$profile) {
            if (!isset($profile->error)) {
                //timed to 0 (instant)
                $clan_object = $fresh_list[clean($profile->name)];
                $profile = (object)array_merge((array)$profile, (array)$clan_object);
                //timed to 0.06 (not many updates), 0.1 (medium updates) or 0.21 (lots of updates)
                
                $update_container['skills'][] = prep_skills($skills, $achievement_types, $clan_last_exp_logs, $profile);
                //timed to 0~0.001, might spike to 0.03 with lots of drops
                $player_drops = prep_drops($profile, $clan, $clan_last_drops);
                $update_container['drops'][] = $player_drops;
                //timed to 0.01s

                if($profile->totalxp > $local_data[$profile->id]->pl_total_exp || !empty($player_drops)){
                    update_player_activity($profile->id, time());
                }
                update_player($profile->id, $profile->clan_rank, $profile->clan_xp, $profile->clan_kills, $profile->totalskill, $profile->questscomplete, $profile->totalxp, $profile->rank, $profile->combatlevel, time(), 0);
                unset($to_update[clean($profile->name)]);
            } else {
                $private_profiles++;
            }
        }

        echo "[done - ".round(microtime(true)-$loop_start, 2)."s]\r\n";
        $start = microtime(true);
        echo "* Falling back to hiscores for " . count($to_update) . " players.. ";
        $fallback_data = $r->get_bulk_hiscores(array_keys($to_update));
        foreach ($to_update as $player_name => $profile_id) {


            $player = (object)$fresh_list[$player_name];

            $total_level = 0;
            $total_exp = 0;
            $total_rank = 0;

            if (isset($fallback_data[$player_name]) && isset($fallback_data[$player_name]->skillvalues)) {
                $profile = new stdClass();
                $profile->id = $profile_id;
                $profile->skillvalues = array();

                $hiscores = $fallback_data[$player_name];
                $total_level = $hiscores->skillvalues['Overall']->level;
                $total_exp = $hiscores->skillvalues['Overall']->xp;
                $total_rank = $hiscores->skillvalues['Overall']->rank;

                foreach ($skills as $sk) {
                    foreach ($hiscores->skillvalues as $skill_name => $skill_details) {
                        if ($sk->sk_name === $skill_name) {
                            $data = new stdClass();
                            $data->level = $skill_details->level;
                            $data->xp = $skill_details->xp;
                            $data->rank = $skill_details->rank;
                            $data->id = $sk->sk_id;
                            $profile->skillvalues[] = $data;
                        }
                    }
                }

                // Prep Skills
                $update_container['skills'][] = prep_skills($skills, $achievement_types, $clan_last_exp_logs, $profile);
            }

            if($total_exp > $local_data[$profile->id]->pl_total_exp){
                update_player_activity($profile->id, time());
            }

            // Update the player.
            update_player($profile_id, $player->clan_rank, $player->clan_xp, $player->clan_kills, $total_level, 0, $total_exp, $total_rank, 0, time(), 1);
        }


        echo "[done - ".round(microtime(true)-$start, 2)."s]\r\n";
        $start = microtime(true);
        echo '* Starting skill & exp log transaction: '.count($to_update).'(original) - '.$private_profiles.'(private) = '.count($update_container['skills']).".. ";
        update_skills($update_container['skills']);
        echo "[done - ".round(microtime(true)-$start, 2)."s]\r\n";
        $start = microtime(true);
        echo '* Starting drops transaction: '.count($update_container['drops']).".. ";
        add_drops($update_container['drops']);
        echo "[done - ".round(microtime(true)-$start, 2)."s]\r\n";
    }else{
        //get_indexed_clan_list returned an error (CLAN NOT FOUND) -> delete clan
        delete_clan($clan->cl_id);
    }
}

function update_plebs()
{
    $r = new \Ralph\api();
    $skills = get_skills();
    $achievement_types = get_achievement_types();

    $local_list = get_clan_members(0);
    $clan_last_exp_logs = get_clan_last_exp_logs(0);
    $clan_last_drops = get_clan_last_drops(0);

    $to_update = []; //determine if a player needs an update (either new or stale)
    foreach ($local_list as $local_player) {
        if ($local_player->pl_last_updated == '0' || $local_player->pl_last_updated < strtotime("-30 minutes")) {
            $to_update[clean($local_player->pl_name)] = $local_player->pl_id;
        }
    }

    $profiles = $r->get_list_profiles($to_update);

    $private_profiles = 0;
    $update_container = [];
    $update_container['skills'] = [];
    $update_container['drops'] = [];
    foreach ($profiles as &$profile) {
        if (!isset($profile->error)) {

            $update_container['skills'][] = prep_skills($skills, $achievement_types, $clan_last_exp_logs, $profile);
            $update_container['drops'][] = prep_drops($profile, null, $clan_last_drops);
            update_player($profile->id, 0, 0, 0, $profile->totalskill, $profile->questscomplete, $profile->totalxp, $profile->rank, $profile->combatlevel, time());
        } else {
            $private_profiles++;
        }
    }
}

/* ============================================
            Update prep functions
============================================ */

function prep_skills($skills, $achievement_types, $clan_last_exp_logs, $profile)
{
    //  Get today's date
    $today = date('Y-m-d');

    //  Personal fallback for when the player doesn't appear in the clan fetched list
    if(!isset($clan_last_exp_logs[$profile->id])){
        $last_exp_logs = false;
    }else{
        $last_exp_logs = $clan_last_exp_logs[$profile->id];//0.009~0.02
    }

    //  Get the new skill values
    $new_skill_values = $profile->skillvalues;

    //  Prep an empty array to store all the records that need to be added/updated
    $update_container = (object)[];
    $update_container->exp_logs_to_update = [];
    $update_container->exp_logs_to_add = [];
    $update_container->achievements_to_add = [];

    //match the RM skillvalues object to the respective skill and merge them
    foreach ($skills as &$sk) {
        foreach ($new_skill_values as $new_skill_values_object) {
            if ($sk->sk_id == $new_skill_values_object->id) {
                $sk = (object)array_merge((array)$sk, (array)$new_skill_values_object);
            }
        }
        $sk->name = $sk->sk_name;
        unset($sk->sk_name);
        unset($sk->sk_id); //unset the sk_name/sk_id to make it more readable
    }
    
    //  Start looping over the skills
    foreach ($skills as $skill) { 
 
        //  Sometimes player wont have any ranks. Fix this.
        if (!isset($skill->rank)) {
            $skill->rank = 0;
        }

        //  Check if that user has experience logs for today -> if not, add one for the current skill
        if (!$last_exp_logs) {
            $ex_add_object = (object)[];
            $ex_add_object->player_id = $profile->id;
            $ex_add_object->skill = $skill;
            $ex_add_object->timestamp = time();
            $update_container->exp_logs_to_add[] = $ex_add_object;
        } else {
            //  If he does have exp logs for today, check if he has one for this specific skill
            $last_exp_log = $last_exp_logs[$skill->id];

            //  If there is no exp log, this is the first time we're logging this skill for the current day
            if (!isset($last_exp_log)) {

                $ex_add_object = (object)[];
                $ex_add_object->player_id = $profile->id;
                $ex_add_object->skill = $skill;
                $ex_add_object->timestamp = time();
                $update_container->exp_logs_to_add[] = $ex_add_object;

            //  If there is an exp log, we have to update it
            } else {

                //  But first, check if it's worth updating
                if ($last_exp_log->ex_value != $skill->xp || $last_exp_log->ex_level != $skill->level || $last_exp_log->ex_rank != $skill->rank) {
                    $ex_update_object = (object)[];
                    $ex_update_object->player_id = $profile->id;
                    $ex_update_object->skill = $skill;
                    $update_container->exp_logs_to_update[] = $ex_update_object;                 
                }
            }
        }
    }

    return $update_container;
}

function prep_drops($profile, $clan, $clan_last_drops)
{
  
    $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    $logs = $profile->activities;
    if(isset($clan_last_drops[$profile->id])){
        $last_log = $clan_last_drops[$profile->id][0];
    }else{
        $last_log = false;
    }
    $drops_to_add = [];

    //  Track visits & caps
    if(!is_null($clan)){
        $visit_message = "Visited my Clan Citadel.";
        $cap_message = "Capped at my Clan Citadel.";
        foreach ($logs as $log) {
            //get clan reset from clan settings
            $reset_date = date('l ga', strtotime($weekdays[$clan->cl_reset_day].' '.$clan->cl_reset_hour.':00'));
            $last_reset = strtotime('last '.$reset_date);
            if($log->text == $visit_message && strtotime($log->date) >= $last_reset){
                set_visit($profile->id, strtotime($log->date));
            }
            if($log->text == $cap_message && strtotime($log->date) >= $last_reset){
                cap($profile->id, strtotime($log->date));
            }
        }
    }

    if (!$last_log) { //no last log, save all
        foreach ($logs as $log) {

            $exploded = explode(' ', strtolower($log->text));
            if (validate_drop_entry($log->text)) {
                $dr_add_object = (object)[];
                $dr_add_object->player_id = $profile->id;
                $dr_add_object->drop = $log;
                $drops_to_add[] = $dr_add_object;
            }
        }
    } else {
        $last_log_found = false;

        foreach (array_reverse($logs) as $log_index => $log) { //array_reverse so you start the loop at the bottom, with the oldest log
            
            if ($last_log_found == true) {
                $exploded = explode(' ', strtolower($log->text));
                if (validate_drop_entry($log->text)) {
                    $dr_add_object = (object)[];
                    $dr_add_object->player_id = $profile->id;
                    $dr_add_object->drop = $log;
                    $drops_to_add[] = $dr_add_object;
                }
            }else{
                if (match_logs($last_log, $log)) {
                    //echo 'Match found at index '.$log_index."\r\n";
                    $last_log_found = true;
                }
            }
        }
        //looped over all the logs and couldnt find the last log -> add all new logs
        if ($last_log_found == false) {
            foreach ($logs as $log) {
                //filter on 'I found'
                $exploded = explode(' ', strtolower($log->text));
                if (validate_drop_entry($log->text)) {
                    $dr_add_object = (object)[];
                    $dr_add_object->player_id = $profile->id;
                    $dr_add_object->drop = $log;
                    $drops_to_add[] = $dr_add_object;
                }
            }
        }
    }

    return $drops_to_add;
}

/* ============================================
        Accoount/player verification
============================================ */

function generate_world($usemembers)
{
    $non_playable_worlds = [13, 15, 47, 48, 52, 55, 75, 90, 93, 94, 95, 101, 102, 107, 109, 110, 111, 112, 113, 121, 122, 125, 126, 127, 128, 129, 130, 131, 132, 133];

    $free_to_play_worlds = [3, 7, 8, 11, 17, 19, 20, 29, 33, 34, 38, 41, 43, 57, 61, 80, 81, 108, 120, 135, 136, 141];

    if($usemembers == true)
    {
        do {   
            $world = rand(1,141);

        } while((in_array($world, $free_to_play_worlds) || in_array($world, $non_playable_worlds)));
       
        return $world;
    }
    else
    {
        do {   
             $world = rand(1,141);

        } while(!in_array($world, $free_to_play_worlds));
        
        return $world;
    } 
}

function check_world($player_name, $world)
{
    $r = new \Ralph\api();
    $login = $r->set_pug("rsthrowaway6969@gmail.com", "thisisapassword");
    if ($login) {
        $details = $r->get_details($player_name);
        if (isset($details->world)) {
           $rm_world = explode(" ", $details->world)[1];
            if ($rm_world == $world) {
                return true;
            } else {
                return 'Player found, but not on the correct world.';
            } 
        } else {
            return 'Could not verify world. Player offline.';
        }  
    } else {
        return 'Could not verify world. Status checker offline.';
    }
}

/* ============================================
        cUrl/Guzzle grab functions
============================================ */

/**
* build_clan_list
* @param integer $depth | number of pages to scrape clan names from (each page is 20 clans)
* @return array $clan_list | list of clan names as strings
*/
function build_clan_list($depth)
{
    $old_clan_list = get_clans();

    for ($i=1; $i <= $depth; $i++) { 
        $dom = parse_page('http://services.runescape.com/m=clan-hiscores/ranking?ranking=xp_total&page='.$i);
        $xpath = new DOMXPath($dom);
        $content = $xpath->query('//div[@class="tableWrap"]/table[2]/tbody/tr/td[@class="col2"]/a/text()[2]');
        foreach ($content as $clan) {
            $list[] = trim($clan->nodeValue);
        }
    }

    foreach ($list as $clan_name) {
        if (!in_array($clan_name, $old_clan_list)) {
            add_clan($clan_name);
        }
    }
}

/**
* fetch_seasonals
* @param integer $depth | number of pages to scrape clan names from (each page is 20 clans)
* @return array $clan_list | list of clan names as strings
*/
function fetch_seasonals()
{
    $r = new \Ralph\api();
    $scraped = $r->scrape_seasonals();

    foreach ($scraped as $scrape_index => $scraped_seasonal) {
        $db_seasonals = get_seasonals();

        $name = $scraped_seasonal->name;
        $winner = $scraped_seasonal->winner;
        $timestamp = $scraped_seasonal->timestamp;

        $ti_id = null;
        $se_id = null;

        if (!in_array($name, array_column($db_seasonals, "se_name"))) {
            $ti_id = add_title();
            $se_id = add_seasonal($name, $ti_id);
            echo "Created new seasonal $se_id for $name\r\n";
        } else {
            foreach ($db_seasonals as $db_seasonal) {
                if ($db_seasonal->se_name === $name) {
                    $ti_id = $db_seasonal->se_ti_id;
                    $se_id = $db_seasonal->se_id;
                }
            }
        }

        $player_id = get_player_id($winner);

        if ($player_id === false) {
            echo "Player $winner not found in database.\r\n";
            continue;
        }

        $player_titles = get_seasonal_player_titles($player_id);
        $player_seasonals = get_player_seasonals($player_id);

        foreach ($player_seasonals as $player_seasonal) {
            if ($player_seasonal->se_name === $name && $player_seasonal->ps_timestamp === $timestamp) {
                echo "Player already registered as the winner of this seasonal.\r\n";
                continue 2;
            }
        }

        echo "Creating seasonal winner for player $player_id has won seasonal $se_id for $timestamp.\r\n";
        add_player_seasonal($player_id, $se_id, $timestamp);

        foreach ($player_titles as $index => $player_title) {
            if ($player_title->se_id === $se_id) {
                echo "Player $player_id already has the title for seasonal $se_id.\r\n";
                continue 2;
            }
        }

        echo "Granting player $player_id the title $ti_id for winning seasonal $se_id.\r\n";
        add_player_title($player_id, $ti_id);
    }
}

/* ============================================
                Password reset
============================================ */

function reset_password($user)
{
    $reset_hash = sha1("runelogs_random_key".$user->us_email);
    send_password_reset_email($user, $reset_hash);
}

/* ============================================
                Email functions
============================================ */

/**
* send_activation_email
* @param string $email | email adress to send the activation mail to
* @param string $hash | activation hash to include in the email
* @return Returns TRUE if the mail was successfully accepted for delivery, FALSE otherwise.
*/
function send_activation_email($email, $hash)
{
    $to      = $email;
    $subject = '[Runelogs] Confirm your new account 🚀';
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    // Additional headers
    $headers .= 'From: Runelogs<accounts@runelo.gs>' . "\r\n";
    $headers .= 'Reply-To: support@runelo.gs' . "\r\n";

    $email_path = __DIR__."/../mail/activation.php";
    ob_start();
    include_once($email_path);
    $mail_body = ob_get_clean();

    return mail($to, $subject, $mail_body, $headers);
}

function send_password_reset_email($user, $reset_hash)
{
    $to      = $user->us_email;
    $subject = '[Runelogs] Reset your password 🔒';
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    // Additional headers
    $headers .= 'From: Runelogs<accounts@runelo.gs>' . "\r\n";
    $headers .= 'Reply-To: support@runelo.gs' . "\r\n";

    $email_path = __DIR__."/../mail/reset.php";
    ob_start();
    include_once($email_path);
    $mail_body = ob_get_clean();

    return mail($to, $subject, $mail_body, $headers);
}

/* ============================================
                helper functions
============================================ */

/**
* parse_page
* @param string $url | The URL to grab & parse as HTML5
* @return returns a HTML5 DOM object
*/
function parse_page($url)
{
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$content = curl_exec($ch);
    $content = mb_convert_encoding($content, 'HTML-ENTITIES', "ISO-8859-1");
	$html5 = new HTML5(array('disable_html_ns' => true));
	return $html5->loadHTML($content);
	curl_close($ch);
}

/**
* match
* @param string $old_name | A player name
* @param string $new_name | A player name
* @return Returns TRUE if the players are very likely to be the same player. False if not.
*/
function match($old_name, $new_name)
{
    $diff = [];
    $r = new \Ralph\api();
    $old_profile = get_player_by_name($old_name);
    $new_profile = $r->get_profile($new_name);
    
    if (!isset($new_profile->error)) {
        //questscomplete
        $diff['quests'] = compare($old_profile->pl_total_quests, $new_profile->questscomplete);
        //total_skill
        $diff['total'] = compare($old_profile->pl_total_skill, $new_profile->totalskill);
        //cb level
        $diff['combat'] = compare($old_profile->pl_combat_level, $new_profile->combatlevel);
        //total exp
        $diff['totalexp'] = compare($old_profile->pl_total_exp, $new_profile->totalxp);

        $match = true;
        foreach ($diff as $percentage) {
            if ($percentage <= 90) {
                $match = false;
            }
        }
        return $match;
    } else {
        return false;
    }  
}

function compare($first_value, $second_value)
{
    return round((1-(abs($first_value - $second_value)/($first_value+1)))*100, 2);
}

function clean($string)
{
    return str_replace(' ', '+', strtolower($string));
}

function clean_underscore($string)
{
    return str_replace(' ', '_', strtolower($string));
}

function format_exp($experience_value)
{
    $length = strlen($experience_value);
    return substr($experience_value, 0, $length - 1);
}

function match_logs($db_log, $rm_log) //first one is from db, second one is from RM
{   
    if ($db_log->dr_title == $rm_log->text && $db_log->dv_timestamp == strtotime($rm_log->date)) {
        return true;
    } else {
        return false;
    }
}

function array_pintersect(array $needles, array $haystack)
{
    foreach ($haystack as $hay) {
        foreach ($needles as $needle) {
            $hay = strval($hay);
            $pos = stripos($hay, $needle);
            if ($pos !== false) {
                return true;
            }
        }
    }
    return false;
}

function contains_special_ignores($log_text, array $edge_cases)
{
    foreach($edge_cases as $edge)
    {
        if(strpos($log_text, $edge) !== false)
        {
            return true;
        }
    }
    
    return false;
}


function validate_drop_entry($log_text)
{
    $ignore = ['trisk', 'effigy', 'battle', 'whip', 'dark', 'Forcae'];
    $ignore_edge_cases = ['dragon boots', 'dragon helm'];
    
    $exploded = explode(' ', strtolower($log_text));
    
    if(in_array('found', $exploded) && !array_pintersect($ignore, $exploded) && !contains_special_ignores($log_text, $ignore_edge_cases))
    {
        return true;
    }
    
    return false;

}

function cache_item_img($drop)
{
    $path_to_file = __DIR__.'/../public/img/items/'.$drop->dr_img_id.'.png';
    if (!file_exists($path_to_file)) {
        try {
            copy('http://services.runescape.com/m=itemdb_rs/obj_big.gif?id='.$drop->dr_img_id, $path_to_file);
        } catch (Exception $e) {
            
        }
    }
}

function cache_avatar($player)
{
    $path_to_file = __DIR__.'/../public/img/players/'.$player->pl_id.'.png';
    if (!file_exists($path_to_file)) {
        try {
            copy('https://services.runescape.com/m=avatar-rs/'.$player->pl_name.'/chat.png', $path_to_file);
        } catch (Exception $e) {
            
        }
    }
}

/* ============================================
                Virtual levels
============================================ */
function get_virtual_level($xp)
{   
    $skill_table = array
    (
        14391160,
        15889109,
        17542976,
        19368992,
        21385073,
        23611006,
        26068632,
        28782069,
        31777943,
        35085654,
        38737661,
        42769801,
        47221641,
        52136869,
        57563718,
        63555443,
        70170840,
        77474828,
        85539082,
        94442737,
        104273167
    );
  
    $i = 0;
    while($i < count($skill_table) && $xp >= $skill_table[$i])
    {
        $i++;
    }

    return 99 + $i;
}

function get_elite_virtual_level($xp)
{
    $skill_table = array
    (
        83370445,
        86186124,
        89066630,
        92012904,
        95025896,
        98106559,
        101255855,
        104474750,
        107764216,
        111125230,
        114558777,
        118065845,
        121647430,
        125304532,
        129038159,
        132849323,
        136739041,
        140708338,
        144758242,
        148889790,
        153104021,
        157401983,
        161784728,
        166253312,
        170808801,
        175452262,
        180184770,
        185007406,
        189921255,
        194927409        
    );

    $i = 0;
    while($i < count($skill_table) && $xp >= $skill_table[$i])
    {
        $i++;
    }

    return 120 + $i;
}

/* RSS Methods */

function get_player_drops_rss($username)
{
    $player = get_player_by_name($username);
    $drops = get_drops($player->pl_id);
    ob_start();
    echo get_rss_header($username);
    foreach($drops as $drop){
        echo get_rss_format_drop($drop);
    }
    echo get_rss_footer();
    $feed = ob_get_clean();
    return $feed;
}

function get_rss_header($name)
{
    return   
    '<?xml version="1.0" encoding="ISO-8859-1"?>
    <?xml-stylesheet type="text/css" media="screen" href="http://feeds.rssboard.org/~d/styles/itemcontent.css"?>
    <rss xmlns:dc="http://purl.org/dc/elements/1.1" version="2.0">
            <channel>
                <title>RuneLogs - Drop log for ' . $name . '</title>
                <link>https://runelo.gs/profile/' . str_replace(" ", "+", $name) . '</link>
                <description>Runelogs is an online experience tracker &amp; clan management tool for the MMORPG Runescape.</description>
    <language>en-us</language>'; 
}

function get_rss_format_drop($drop)
{
    return "<item>
    <title>$drop->dr_title</title>
    </item>"; 
}

function get_rss_footer()
{
    return "</channel></rss>"; 
    
}