var quests = [];
var filtered_quests = [];

var text_filter = "";

function load_quests()
{
    var quest_body = document.querySelector(".quest-body");
    if(quest_body != null)
    {
        //fill quests variable.
        quests = JSON.parse(quest_body.getAttribute("data-quests"));
        
        //apply event listeners on filters. 
        var title_search = document.querySelector("#quest-text-filter").addEventListener('input', function(e) {
		     
         if(e.target.value.length > 0)
            {
                text_filter = e.target.value;
            }
            else{
                text_filter = "";
            }
        
            apply_filters();
        });
        init_check_boxes('quest-diff-filter');
        init_check_boxes('quest-member-filter');
        init_check_boxes('quest-status-filter');
        init_drop_downs(".quest-title-trigger");
        init_drop_downs(".quest-diff-trigger");
        init_drop_downs(".quest-member-trigger");
        init_drop_downs(".quest-status-trigger");
    }
}

function initiate_quests()
{
   if(quests.length == 0)
    {
        load_quests();
    } 
}

function init_check_boxes(name)
{
     var member_filters = document.querySelectorAll('input[name="' + name + '"]');
        
        for (var i = 0; i < member_filters.length; i++)
        {
            member_filters[i].addEventListener('change', function(e) {
                
                apply_filters();
            });
            
            //member_filters[i].checked = true;		            
        }
}

function init_drop_downs(cName)
{
    var elem = document.querySelector(cName); elem.addEventListener('click', function() {
		 elem.parentElement.classList.toggle('is-active');
	});
}

function apply_filters()
{
    filtered_quests = quests;
    
    var new_arr = [];
    
    var play_types = get_selected_text_types('quest-member-filter');
    var status_types = get_selected_text_types('quest-status-filter');
    var difficulty_types = get_selected_difficulty_types();
    
    for(var i = 0, len = filtered_quests.length; i < len; i++)
    {
        var quest = filtered_quests[i];  
        
        //Check if the quest has the wanted completion type and is selected.
        if(status_types.length > 0 && status_types.indexOf(quest.status) == -1)
        {
            continue;
        }
        
        //Check if the quest is p2p/f2p and is selected.
        if(play_types.length > 0 && play_types.indexOf(quest.members.toString()) == -1){
            
            continue;
        }
        
        //Check if the quest has the wanted difficulty and is selected.
        if(difficulty_types.length > 0 && difficulty_types.indexOf(quest.difficulty) == -1){
            
            continue;
        }
        
        //Check if the title matches the search given in the title search.
        if(quest.title.toLowerCase().indexOf(text_filter.toLowerCase()) == -1)
        {
            continue;
        }
            
        
        new_arr.push(quest);
    }
    
    filtered_quests = new_arr;
    filter_quests();
}

function get_selected_text_types(name)
{
    var checked_types  = document.querySelectorAll('input[name="' + name + '"]:checked');
    
    var type_values = [];
    for(var i = 0; i < checked_types.length; i++)
    {           
        type_values.push(checked_types[i].value);
    }
    
    return type_values;
}

function get_selected_difficulty_types()
{
    var checked_difficulties  = document.querySelectorAll('input[name="quest-diff-filter"]:checked');
    
    var difficulty_values = [];
    for(var i = 0; i < checked_difficulties.length; i++)
    {           
        difficulty_values.push(parseInt(checked_difficulties[i].value));
    }
    
    return difficulty_values;
}

function filter_quests()
{
    var filter_titles = filtered_quests.map(function(a) {return a.title;});
    var table_body = document.querySelector(".quest-body");
    for(var i = 0; i < table_body.rows.length; i++)
    {
        var row = table_body.rows[i];
        if(filter_titles.indexOf(row.getElementsByTagName('b')[0].textContent) == -1)
        {
            row.classList.add("hidden");
        }
        else
        {
            row.classList.remove("hidden");
        }
    }
  
}

initiate_quests();


    
