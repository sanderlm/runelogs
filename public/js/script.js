/* ================================================================
					Runelogs main Javascript file.
Rules:
	* Don't use jQuery
	* Don't use inline events
	* Use querySelector
	* Write as little JS as possible
=================================================================*/

/*
	Datepickers (global)
*/
function apply_datepickers(){
	var datepickers = document.querySelectorAll('.datepicker');
	for ( var i=0, len = datepickers.length; i < len; i++ ) {
		var datepicker = datepickers[i];
		new Pikaday({ field: datepicker,format: 'DD-MM-Y' });
	}
}

/*
	Hamburger menu (mobile)
*/
var burger = document.querySelector('.navbar-burger');
var menu = document.querySelector('.navbar-menu');
burger.addEventListener('click', function() {
	burger.classList.toggle('is-active');
	menu.classList.toggle('is-active');
});

/*
	Skills template custom experience period dropdown
*/
function add_profile_header_handlers(){

	var skill_dropdown = document.querySelector('.skills-dropdown');
	var skill_trigger = document.querySelector('.skills-trigger');
	var skill_dropdown_menu = document.querySelector('.skills-dropdown-menu');
	var skill_dropdown_custom_start = document.querySelector('.custom-period-start');
	var skill_dropdown_custom_end = document.querySelector('.custom-period-end');
	var skill_dropdown_items = document.querySelectorAll('.skill-dropdown-item');

	if(skill_trigger != null){
		skill_trigger.addEventListener('click', function() {
			skill_dropdown.classList.toggle('is-active');
		});

		//	Dropdown items (the predefined links at the top)
		for ( var i=0, len = skill_dropdown_items.length; i < len; i++ ) {
			skill_dropdown_items[i].addEventListener('click', function() {
				var time_period = this.dataset.period;
				var result = get_exp_period(time_period);
			});
		}

		//	Custom time period (2 dates)
		skill_dropdown_custom_end.addEventListener('change', function() {
			var start_date = skill_dropdown_custom_start.value;
			var end_date = skill_dropdown_custom_end.value;
			var result = get_exp_period("custom", start_date, end_date);
		});
	}
}

function get_exp_period(period_name, start_date = false, end_date = false)
{
	var skill_dropdown = document.querySelector('.skills-dropdown');
	var skill_dropdown_text = document.querySelector('.skills-dropdown-text');

	var clean_strings = {
		"current-week": "This week",
		"last-week": "Last week",
		"last-month": "This month",
		"custom": "Custom"
	}
	skill_dropdown_text.innerHTML = clean_strings[period_name];
	var player_id = skill_dropdown.dataset.player;
	if(period_name == "custom"){
		//send custom request with 2 dates
		var data = {
			player_id: player_id,
			period: "custom",
		    start_date: skill_dropdown_custom_start.value,
		    end_date: skill_dropdown_custom_end.value
		};
	}else{
		//send regular request with period name
		var data = {
			player_id: player_id,
			period: period_name
		};
	}
	//fire the request
	var exp_request = new XMLHttpRequest();
	exp_request.open('POST', '/api/get_time_period_data', true);
	exp_request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
	exp_request.send(JSON.stringify(data));
	exp_request.onreadystatechange = function() {
	    if(exp_request.readyState == 4 && exp_request.status == 200) {
	        display_exp_period_data(JSON.parse(exp_request.responseText));
	        skill_dropdown.classList.toggle('is-active');
	    }
	}
}

function display_exp_period_data(result)
{
	var dynamic_overall = document.querySelector('.dynamic-overall');
	var dynamic_column = document.querySelectorAll('.dynamic');
	var total = 0;
	if(result[0] != undefined){   
	    for ( var i=0, len = dynamic_column.length; i < len; i++ ) {

			if(result[i].experience != 0){
				dynamic_column[i].classList = "dynamic orange bold";
				var exp = result[i].experience.toString().substr(0, result[i].experience.toString().length - 1);
			}else{
				dynamic_column[i].classList = "dynamic";
				var exp = result[i].experience;
			}

			total += parseInt(exp);
			dynamic_column[i].innerHTML = number_format(exp);
		}
		if(total != 0){
			dynamic_overall.classList = "dynamic-overall orange bold";
		}else{
			dynamic_overall.classList = "dynamic-overall";
		}

		dynamic_overall.innerHTML = number_format(total);
	}else{
	   for ( var i=0, len = dynamic_column.length; i < len; i++ ) {
	   		dynamic_column[i].classList = "dynamic";
			dynamic_column[i].innerHTML = 0;
		}
		dynamic_overall.classList = "dynamic-overall";
		dynamic_overall.innerHTML = 0;
	}

}

/*
Nightmode toggle (global)
*/
var body = document.querySelector('body');
var is_nightmode = body.classList.contains('is-nightmode');
var night_mode_toggle = document.querySelector('.night-mode-toggle');
var night_mode_icon = night_mode_toggle.querySelector(':scope .fa');
var home_illustration = document.querySelector('.home-illustration img');

function toggle_night_mode(evt = false) {
	if (evt) evt.preventDefault();

	body.classList.toggle('is-nightmode');
	is_nightmode = !is_nightmode;
	night_mode_icon.className = is_nightmode == true ? 'fa fa-sun-o' : 'fa fa-moon-o';
	if (home_illustration) home_illustration.src = is_nightmode == true ? '/img/banners/home_n.png' : '/img/banners/home.png';
	localStorage.setItem("nightmode", JSON.stringify(is_nightmode));

	req_night_mode();
}

function req_night_mode(){
    var request = new XMLHttpRequest();
    request.open('GET', '/toggle-nightmode', true);
    request.onload = function() {
        var resp = request.responseText;
    };

    request.send();
}

if(night_mode_toggle != null){
    night_mode_toggle.addEventListener('click', toggle_night_mode);
}

if (localStorage.getItem("nightmode") === null) {
    localStorage.setItem("nightmode", JSON.stringify(is_nightmode));
} else if (is_nightmode !== JSON.parse(localStorage.getItem("nightmode"))){
	toggle_night_mode();
}

/*
	Server status page
*/
if(window.location.pathname == '/status'){
	var request = new XMLHttpRequest();
	request.open('GET', '/ping', true);

	request.onload = function() {
		var resp = request.responseText;
		var status = JSON.parse(resp);

		Object.keys(status).forEach(function(key) {
				var container = document.querySelector('.'+key);
				container.innerHTML = '';
				if(status == 1 || status == 2){
					container.classList.add('red');
				}else{
					container.classList.add('green');
				}
		    	var span = document.createElement("span");
		    	var textnode = document.createTextNode(status[key]);
		    	span.appendChild(textnode);
		    	container.appendChild(span);
		});
	};
	request.send();
}

/*
	Delete button closes notifications (global)
*/
var close_buttons = document.querySelectorAll('.delete');
if(close_buttons > 0){
	for ( var i=0, len = close_buttons.length; i < len; i++ ) {
		close_buttons[i].addEventListener('click', function(e) {
			e.parentElement.classList.toggle("hidden");
		});
	}
}

/*
	World verification type selection
*/
var verification_world_type = document.querySelectorAll('.world_type_radio');
var world_type_form = document.querySelector("#world_form");

for ( var i=0, len = verification_world_type.length; i < len; i++ ) {
	verification_world_type[i].addEventListener('change', function() {
		console.log('what');
		world_type_form.submit();
	});
}

/*
	Navbar and home search select change placeholder text
*/
var navbar_search_select = document.querySelector('.navbar-search-select');
var navbar_search_text = document.querySelector('.navbar-search .search-input');
navbar_search_select.addEventListener('change', function() {
	navbar_search_text.placeholder = 'Search for a '+this.value+' ..';
});

//Check if we're not on the homepage anymore.
if(window.location.pathname == '/'){
var home_search_select = document.querySelector('.home-search-select');
var home_search_text = document.querySelector('.home-search .search-input');
home_search_select.addEventListener('change', function() {
	home_search_text.placeholder = 'Search for a '+this.value+'..';
});
}

/*
	Profile & clan subnav
*/
var subnav = document.querySelectorAll('.subnav .link');
var sub_content = document.querySelector(".sub-content");
for ( var i=0, len = subnav.length; i < len; i++ ) {

	subnav[i].addEventListener('click', function(e) {
		e.preventDefault();
		var subnav_link = this.querySelector('a');
		var request = new XMLHttpRequest();
		request.open('GET', subnav_link.getAttribute('href'), true);
		sub_content.innerHTML = '<span class="icon is-medium loading-indicator"><i class="fa fa-refresh fa-spin fa-2x"></i></span>';

		request.onload = function() {
			var resp = request.responseText;
			sub_content.innerHTML = resp;
			apply_dynamic_handlers();
		};

		request.send();
		subnav.forEach(function(el) {
		  el.classList.remove("is-active")
		})
		this.classList.toggle('is-active');
	});
}

/*
	Clan dashboard
*/
var dashboard_nav = document.querySelectorAll('.dashboard-nav .link');
var dashboard_content = document.querySelector(".sub-content");
for ( var i=0, len = dashboard_nav.length; i < len; i++ ) {

	dashboard_nav[i].addEventListener('click', function(e) {
		e.preventDefault();
		var dashboard_nav_link = this.querySelector('a');
		var request = new XMLHttpRequest();
		request.open('GET', dashboard_nav_link.getAttribute('href'), true);

		request.onload = function() {
			var resp = request.responseText;
			dashboard_content.innerHTML = resp;
			apply_dynamic_handlers();
		};

		request.send();
		dashboard_nav.forEach(function(el) {
		  el.classList.remove("is-active")
		})
		this.classList.toggle('is-active');
	});
}

function add_pagination_event_handlers(){
	var pagination_links = document.querySelectorAll('.pagination-link');
	var sub_content = document.querySelector(".sub-content");
	for ( var i=0, len = pagination_links.length; i < len; i++ ) {

		pagination_links[i].addEventListener('click', function(e) {
			e.preventDefault();
			var request = new XMLHttpRequest();
			request.open('GET', this.getAttribute('href'), true);

			request.onload = function() {
				var resp = request.responseText;
				sub_content.innerHTML = resp;
				apply_dynamic_handlers();
			};

			request.send();
		});
	}
}

order = "desc";

function add_sorting_event_handlers(){
	var member_table_sorts = document.querySelectorAll('.member-table-sort');
	var sub_content = document.querySelector(".sub-content");
	for ( var i=0, len = member_table_sorts.length; i < len; i++ ) {

		member_table_sorts[i].addEventListener('click', function(e) {
			e.preventDefault();
			var clan = this.parentNode.dataset.clan;
			order = order == "desc" ? "asc" : "desc";
			var data = "sort="+this.dataset.sort+"&order="+order;
			var request = new XMLHttpRequest();
			request.open('GET', '/clan/'+clan+'/members/1/'+this.dataset.sort+'/'+order, true);

			request.onload = function() {
				var resp = request.responseText;
				sub_content.innerHTML = resp;
				apply_dynamic_handlers();
			};

			request.send();

		});
	}
}

function add_dashboard_sorting_event_handlers(){
	var member_table_sorts = document.querySelectorAll('.dashboard-table-sort');
	var sub_content = document.querySelector(".sub-content");
	for ( var i=0, len = member_table_sorts.length; i < len; i++ ) {

		member_table_sorts[i].addEventListener('click', function(e) {
			e.preventDefault();
			var clan = this.parentNode.dataset.clan;
			order = order == "desc" ? "asc" : "desc";
			var data = "sort="+this.dataset.sort+"&order="+order;
			var request = new XMLHttpRequest();
			request.open('GET', '/dashboard/'+clan+'/members/1/'+this.dataset.sort+'/'+order, true);

			request.onload = function() {
				var resp = request.responseText;
				sub_content.innerHTML = resp;
				apply_dynamic_handlers();
			};

			request.send();

		});
	}
}

function apply_dynamic_handlers(){
	add_pagination_event_handlers();
	add_sorting_event_handlers();
	add_dashboard_sorting_event_handlers();
	add_profile_header_handlers();
	apply_datepickers();
}

apply_dynamic_handlers();

/*
	Helper functions
*/

function number_format(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}