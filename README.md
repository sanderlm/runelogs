# Runelo.gs

Runelo.gs is an experience tracking website for the Jagex MMORPG Runescape. It allows players to track the experience they gain, work towards goals, save drops & achievements they get, etc..

The secondary goal is to be a clan management solution for clan leaders, offering features like automatic name change tracking and citadel visits/caps.

## Stack
* Database: MariaDB + PDO driver to PHP
* PHP framework: [Slim 3](https://www.slimframework.com/) (routing & dependency injection)
* Templates: [Twig](https://twig.symfony.com/)
* CSS framework: [Bulma](http://bulma.io/documentation/overview/start/)
* Ralph for all the web facing Runescape related requests

I chose Slim over something larger like Laravel because I wanted to keep this project as simple as possible. I imagined the update logic to be seperated from the Slim app into a class that can be cloned onto the slaves.
These functions would run as crons and send the updated data back to the main server.
Ideally the Slim app (this repo) running on the web server would only have to pull data from the database, serve routes and display the correct template.

## Set up
1. Download or clone repo
2. Run composer install
3. Use the included SQL file to build the database locally (prefered name: runelogs)
4. Enter your local database user/pw in the conf.d folder (parameters.yml)
5. Run the following command to launch a dev server
```
php -S localhost:8080 -t public public/index.php
```
5. (optional) Move into the /cron/ folder and run the following commands
```
php build_clan_list.php
```
and
```
php update.php (will add players, skill_values, drops, etc.. for every clan in the 'clans' table)
```

## Slave setup
* Spin up a new DigitalOcean droplet (private networking enabled & SSH keys added, Ubuntu 16.04)
* Install PHP, zip/unzip, composer, mariadb-server, php7.0-xml, php7.0-curl, php7.0-mysql
* FileZilla into the droplet, make a folder 'rl' in /root
* Upload the content of 'rl_updater' to the 'rl' folder
* Run composer install inside /root/rl
* On master, enter the mysql cli  as root and enter the following commands:
>create user 'sander'@'new slave private ip' identified by 'password';
>grant all privileges on runelogs.* to 'sander'@'new slave private ip';
>flush privileges;
* On slave: crontab -e and enter '*/5 * * * * php /root/rl/update.php > /root/rl/update.log'