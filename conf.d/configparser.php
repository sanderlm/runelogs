<?php

require __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 23/09/2017
 * Time: 10:59
 */

function getParameter($param=null, $delimiter=".", $filename=__DIR__ . '/../conf.d/parameters.yml')
{
    if (!file_exists($filename)) {
        throw new \Exception("$filename does not exist.");
    }

    $configuration = Yaml::parse(file_get_contents($filename));

    if (!isset($param)) {
        return $configuration;
    }

    # database.user => ['database']['user'].
    foreach (explode($delimiter, $param) as $step) {
        if (!isset($configuration[$step])) {
            throw new \Exception("Parameter $param not found in $filename.");
        }

        $configuration = $configuration[$step];
    }

    return $configuration;
}