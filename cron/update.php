<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../conf.d/configparser.php';
require __DIR__ . '/../src/db.php';
require __DIR__ . '/../src/controller.php';
require __DIR__ . '/../src/lib/ralph.php';

$r = new \Ralph\api();

/*
This script is set to run hourly in the crontab
*/

//	1. Grab the config json from the master endpoint
$config = $r->get_json('https://runelo.gs/config');

//	2. Use the config update_order to calc which clans this shard should update
$server_name = "master";
$server = get_server($server_name);
$clans = get_clans();

$total_clan_count = count($clans);

$shard_count = count($config);
$shard_index = array_search($server_name, $config);
$shard_update_count = ceil($total_clan_count / $shard_count);

$start_index = $shard_index * $shard_update_count;//e.g: 0, 100, 200, 300
$end_index = ($start_index + $shard_update_count) - 1;//e.g: 99, 199, 299, 399

recheck_plebs();
$update_start = microtime(true);
update_server_start($server->sr_id, $update_start);

//	4. Loop over those clans, and update them
for ($i=$start_index; $i <= $end_index; $i++) {
	
	if(isset($clans[$i])){
		$clan = $clans[$i];
		$clan_update_start = microtime(true);
    	try {
	      roster_update($clan); //new people, leavers, name changes, clan changes
	      data_update($clan); //update skill_values, exp_logs, drops & drop_values
	      echo "* ".$clan->cl_name." has been updated. [total time: ".round(microtime(true)-$clan_update_start, 2)."s]\r\n";
	    } catch (Exception $e) {
	        $update_end = microtime(true);
	        update_server_end($server, $update_end, false);
	        die("!!!! Clan update halted at clan #".$clan->cl_id." with name ".$clan->cl_name.". (".$e->getMessage().")");
	    }
	}
}

//update_plebs();

$update_end = microtime(true);
update_server_end($server, $update_end, true);
echo 'Update completed';
exit();