<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../src/db.php';
require __DIR__ . '/../src/controller.php';
/*
This script is set to run hourly in the crontab
*/
build_clan_list(1);
exit();