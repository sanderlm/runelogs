<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../conf.d/configparser.php';
require __DIR__ . '/../src/db.php';
require __DIR__ . '/../src/controller.php';
require __DIR__ . '/../src/lib/ralph.php';

/*
This script is set to run in the background, using exec(), when a profile is visited.
*/
player_update($argv[1]);
exit();